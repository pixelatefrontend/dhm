import type { TContactForm } from '$lib/types';
import type { RequestEvent, RequestHandlerOutput } from '@sveltejs/kit/types/internal';
import { IRateLimiterOptions, RateLimiterMemory } from 'rate-limiter-flexible';
import mailer from 'nodemailer';
import { mailBuilder } from '$lib/helpers/mail.helper';

const rateLimitOptions: IRateLimiterOptions = {
	points: 6,
	duration: 1,
};

const rateLimiter = new RateLimiterMemory(rateLimitOptions);

const smtpTransport = mailer.createTransport({
	host: import.meta.env.VITE_MAIL_HOST,
	port: parseInt(import.meta.env.VITE_MAIL_PORT),
	auth: {
		user: import.meta.env.VITE_MAIL_USERNAME,
		pass: import.meta.env.VITE_MAIL_PASSWORD,
	},
	tls: {
		rejectUnauthorized: false,
	},
});

const get = (): RequestHandlerOutput => {
	return {
		headers: {
			location: '/',
		},
		status: 302,
	};
};

const post = async ({ clientAddress, request }: RequestEvent): Promise<RequestHandlerOutput> => {
	const data: TContactForm = await request.json();

	try {
		await rateLimiter.consume(clientAddress, 2);
		await smtpTransport.sendMail({
			from: `${data.name} <${import.meta.env.VITE_MAIL_USERNAME}>`,
			to: 'onder.bakirtas@pixelate.com.tr, iletisim@dijitalhafiza.com',
			subject: `${data.name} - Dijital Hafıza Merkezi İletişim Formu`,
			html: mailBuilder(data),
		});

		return {
			body: {
				ok: true,
				message: 'İletişim formunu aldık, teşekkür ederiz.',
			},
		};
	} catch (error) {
		if (error.remainingPoints === 0) {
			return {
				status: 429,
				body: {
					ok: false,
					message: 'Sakin ol ve yavaşça o klavyeyi masaya bırak.',
				},
			};
		}
		return {
			status: 500,
			body: {
				ok: false,
				message: error,
			},
		};
	}
};

export { get, post };
