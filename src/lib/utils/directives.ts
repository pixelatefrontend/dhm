import type { TAction } from '$lib/types';

const clickAway = (node: HTMLElement, callback: CallableFunction): TAction => {
	const handleClick = (event: MouseEvent) => {
		const target = event.target as Element;
		if (node && !node.contains(target)) {
			callback();
		}
	};

	document.addEventListener('click', handleClick, true);

	return {
		update(newCallback) {
			callback = newCallback;
		},

		destroy() {
			document.removeEventListener('click', handleClick, true);
		}
	};
};

export { clickAway };
