import { API_STORE } from '../constants/urls';

type UrlifyParams = {
	section: keyof typeof API_STORE;
	slug?: string;
	fields?: string;
	page?: number;
	exclude?: number;
	perPage?: number;
};

const urlify = ({ section, slug, fields, page, exclude, perPage }: UrlifyParams): string => {
	let url = API_STORE[section] + '?';
	if (slug) {
		url = url + 'slug=' + slug;
		if (fields) url = url + '&_fields=' + fields;
	} else {
		if (fields) url = url + '_fields=' + fields;
		if (page) url = url + '&page=' + page;
		if (exclude) url = url + '&exclude=' + exclude
		if (perPage) url = url + '&per_page=' + perPage
	}

	return url;
};

export { urlify };
