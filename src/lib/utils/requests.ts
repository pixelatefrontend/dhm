import type { CategoryItem, Section } from '$lib/constants/categories';
import { CATEGORIES } from '$lib/constants/categories';
import type {
	ArtworkResponse,
	CategoryResponse,
	DocumentaryResponse,
	ExhibitionResponse,
	InfographicResponse,
	MapResponse
} from '$lib/response.type';
import type { Artist, Documentary, Exhibition, IInfographic, IMap, MapTypes } from '$lib/types';
import { urlify } from '.';

const getExhibitions = async (categoryId: number): Promise<Exhibition[]> => {
	const request = await fetch(
		urlify({
			section: 'exhibitions',
			fields: `title,meta_data,slug&exhibition_categories=${categoryId}`,
			perPage: 12,
			page: 1
		})
	);

	const response: ExhibitionResponse[] = await request.json();

	const exhibitions: Exhibition[] = response.map((e) => {
		const result: Exhibition = {
			title: e.title.rendered,
			subtitle: e.meta_data.exhibition_categories[0].name,
			slug: e.slug
		};

		if (e.meta_data.image_details.thumbnail.url) {
			result.thumbnail = e.meta_data.image_details.thumbnail.url;
		}
		return result;
	});

	return exhibitions;
};

const getArtists = async (): Promise<Artist[]> => {
	const request = await fetch(
		urlify({
			section: 'artists',
			fields: `title,meta_data`,
			perPage: 12,
			page: 1
		})
	);

	const response: ArtworkResponse[] = await request.json();

	const artists: Artist[] = response.map((e) => {
		const result: Artist = {
			title: e.title.rendered,
			subtitle: 'Sanatçı',
			slug: e.slug
		};

		if (e.meta_data.image_details.thumbnail.url) {
			result.thumbnail = e.meta_data.image_details.thumbnail.url;
		}
		return result;
	});

	return artists;
};

const getCategory = async (appSection: Section): Promise<CategoryItem[]> => {
	const section = CATEGORIES.find((e) => e.section === appSection).handler;
	const fields = 'id,name,slug';

	const request = await fetch(urlify({ section, fields }));
	const response: CategoryResponse[] = await request.json();

	// Return empty array if no categories found
	if (response.length === 0) return [];

	const result = response.map((r) => ({ id: r.id, name: r.name, slug: r.slug }));

	return result;
};

type DocumentaryParams = {
	page: number;
	categoryId?: number;
};

type DocumentaryResult = {
	documentaries: Documentary[];
	hasNext: boolean;
};

const getDocumentaries = async ({
	page,
	categoryId
}: DocumentaryParams): Promise<DocumentaryResult> => {
	let fields = `title,meta_data`;
	if (categoryId) fields = fields + '&documentary_categories=' + categoryId;
	const request = await fetch(
		urlify({
			section: 'documentaries',
			perPage: 9,
			fields,
			page
		})
	);

	const response: DocumentaryResponse[] = await request.json();

	const documentaries: Documentary[] = response.map((r) => {
		const result: Documentary = {
			id: r.id,
			slug: r.slug,
			title: r.title.rendered,
			producer: r.meta_data.yapimci
		};

		if (r.meta_data.image_details.thumbnail.url) {
			result.thumbnail = r.meta_data.image_details.thumbnail.url;
		}

		return result;
	});

	const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== page;

	return { documentaries, hasNext };
};

type MapsParams = {
	type: MapTypes;
	page: number;
};

type MapsResult = {
	maps: IMap[];
	hasNext: boolean;
};

const getMaps = async ({ type, page }: MapsParams): Promise<MapsResult> => {
	let mapCategoryId: number;
	if (type === 'interactive') {
		mapCategoryId = 49;
	} else {
		mapCategoryId = 48;
	}

	const request = await fetch(
		urlify({
			section: 'maps',
			perPage: 12,
			fields: 'id,title,slug,meta_data&map_categories=' + mapCategoryId,
			page
		})
	);
	const response: MapResponse[] = await request.json();

	const maps: IMap[] = response.map((r) => {
		const result: IMap = {
			id: r.id,
			title: r.title.rendered,
			slug: r.slug,
			thumbnail: r.meta_data.kucuk_gorsel.url
		};

		return result;
	});

	const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== page;

	return { maps, hasNext };
};

type InfographicParams = {
	page: number;
	categoryId?: number;
};

type InfographicResult = {
	infographics: IInfographic[];
	hasNext: boolean;
};

const getInfographics = async ({
	page,
	categoryId
}: InfographicParams): Promise<InfographicResult> => {
	let fields = `id,title,slug,meta_data`;
	if (categoryId) fields = fields + '&infographic_categories=' + categoryId;
	const request = await fetch(
		urlify({
			section: 'infographics',
			perPage: 9,
			fields,
			page
		})
	);

	const response: InfographicResponse[] = await request.json();

	const infographics: IInfographic[] = response.map((r) => {
		const result: IInfographic = {
			id: r.id,
			slug: r.slug,
			title: r.title.rendered
		};

		if (r.meta_data.image_details.thumbnail.url) {
			result.thumbnail = r.meta_data.image_details.thumbnail.url;
		}

		return result;
	});

	const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== page;

	return {
		hasNext,
		infographics
	};
};

export { getExhibitions, getArtists, getCategory, getDocumentaries, getMaps, getInfographics };
