type Bio = {
	name: string;
	image: string;
	birth: number | string;
	death: number | string;
	job: string;
};

const BIOS: Bio[] = [
	{
		name: 'Jamal Abdel Rahman Mansour',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-1.png',
		birth: 1913,
		death: 1984,
		job: 'Siyasetçi'
	},
	{
		name: 'Abu Amr Ziad Mahmoud',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-2.png',
		birth: 1972,
		death: 2020,
		job: 'Hamas Kabinesi, Siyasetçi'
	},
	{
		name: 'Ebü’l-Mutarrif Abdurrahmân',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-3.png',
		birth: 731,
		death: 788,
		job: 'Devlet Kurucusu'
	},
	{
		name: 'İbn Meymun',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-4.png',
		birth: 1913,
		death: 1984,
		job: 'Filozof, Tabip'
	},
	{
		name: 'Herakleidos',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-5.png',
		birth: 610,
		death: 641,
		job: 'Filozof, Tabip'
	},
	{
		name: 'Savvaf, Muhammed Mahmûd',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-6.png',
		birth: 1915,
		death: 1992,
		job: 'Alim'
	},
	{
		name: 'Abu Amr Ziad Mahmoud',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-2.png',
		birth: 1972,
		death: 2020,
		job: 'Hamas Kabinesi, Siyasetçi'
	},
	{
		name: 'Savvaf, Muhammed Mahmûd',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-6.png',
		birth: 1915,
		death: 1992,
		job: 'Alim'
	},
	{
		name: 'Ebü’l-Mutarrif Abdurrahmân',
		image: 'http://pixelateworks.com/client/ihh/assets/img/biyografiler/person-3.png',
		birth: 731,
		death: 788,
		job: 'Devlet Kurucusu'
	}
];

type MapCat = {
	icon: string;
	title: string;
	text: string;
	link: string;
	linkText: string;
};

const MAPCAT: MapCat[] = [
	{
		icon: 'sematik-haritalar',
		title: 'Şematik Haritalar',
		text: 'Lorem ipsum sit amet. Dummy metin Filistinli İsrail vatandaşları, vatandaşlık ve oy kullanma hakkı verilmesine rağmen altında yaşadılar.',
		link: '/haritalar/sematik',
		linkText: 'Haritaları Görüntüle'
	},
	{
		icon: 'interaktif-haritalar',
		title: 'İnteraktif Haritalar',
		text: 'Lorem ipsum sit amet. Dummy metin Filistinli İsrail vatandaşları, vatandaşlık ve oy kullanma hakkı verilmesine rağmen altında yaşadılar.',
		link: '/haritalar/interaktif',
		linkText: 'Yakında'
	}
];

type Maps = {
	image: string;
	title: string;
	link: string;
};

const MAPS: Maps[] = [
	{
		image: '/img/haritalar/map-1.png',
		title: 'Batı Şeria Yıkımları Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-2.png',
		title: '1946’da Filistin Nüfus Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-3.png',
		title: 'İşgal Altındaki Filistin Toprakları',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-4.png',
		title: '1946’da Filistin Nüfus Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-5.png',
		title: 'Batı Şeria Yıkımları Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-6.png',
		title: 'Communities And Residents In Area C',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-3.png',
		title: 'İnsanı Durumun En Kritik Olduğu Yerler',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-2.png',
		title: '1946’da Filistin Nüfus Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-4.png',
		title: 'İşgal Altındaki Filistin Toprakları',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-1.png',
		title: 'Batı Şeria Yıkımları Haritası',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-3.png',
		title: 'İnsanı Durumun En Kritik Olduğu Yerler',
		link: '/haritalar'
	},
	{
		image: '/img/haritalar/map-2.png',
		title: '1946’da Filistin Nüfus Haritası',
		link: '/haritalar'
	}
];

type Infographics = {
	image: string;
	title: string;
};

const INFOGRAPHICS: Infographics[] = [
	{
		image: '/img/infografikler/infografik-1.png',
		title: 'Filistin Nüfusu 2019'
	},
	{
		image: '/img/infografikler/infografik-2.png',
		title: 'Batı Şeria’nin İhlak Kararı'
	},
	{
		image: '/img/infografikler/infografik-3.png',
		title: 'Barı Şeria’nın İhlakı'
	},
	{
		image: '/img/infografikler/infografik-4.png',
		title: 'Filistin’in 5 Meselesi'
	},
	{
		image: '/img/infografikler/infografik-5.png',
		title: 'Gazze’de İnsani Kriz'
	},
	{
		image: '/img/infografikler/infografik-1.png',
		title: 'Filistin Nüfusu 2019'
	},
	{
		image: '/img/infografikler/infografik-2.png',
		title: 'Batı Şeria’nin İhlak Kararı'
	},
	{
		image: '/img/infografikler/infografik-3.png',
		title: 'Barı Şeria’nın İhlakı'
	},
	{
		image: '/img/infografikler/infografik-4.png',
		title: 'Filistin’in 5 Meselesi'
	}
];

type Books = {
	image: string;
	name: string;
	author: string;
};

const BOOKS: Books[] = [
	{
		image: '/img/kitaplar/kitap-1.png',
		name: 'Buiding Arafat’s Police',
		author: 'Brynjar Lia'
	},
	{
		image: '/img/kitaplar/kitap-2.png',
		name: '“Yahudi ve Hristiyanlar Cennete Girecek” Diyenler Cennete… Girecek',
		author: 'Ali Eren'
	},
	{
		image: '/img/kitaplar/kitap-3.png',
		name: '100 Soruda Kudüs',
		author: 'Ali İhsan Aydın'
	},
	{
		image: '/img/kitaplar/kitap-4.png',
		name: 'Bitmeyen İhanet',
		author: 'Berdal Aral'
	},
	{
		image: '/img/kitaplar/kitap-5.png',
		name: 'Gazze’ye Yolculuk',
		author: 'Sümeyye Ertekin'
	},
	{
		image: '/img/kitaplar/kitap-6.png',
		name: 'Haritada Kan Lekesi',
		author: 'Asım Öz'
	},
	{
		image: '/img/kitaplar/kitap-1.png',
		name: 'Buiding Arafat’s Police',
		author: 'Brynjar Lia'
	},
	{
		image: '/img/kitaplar/kitap-2.png',
		name: '“Yahudi ve Hristiyanlar Cennete Girecek” Diyenler Cennete… Girecek',
		author: 'Ali Eren'
	},
	{
		image: '/img/kitaplar/kitap-3.png',
		name: '100 Soruda Kudüs',
		author: 'Ali İhsan Aydın'
	},
	{
		image: '/img/kitaplar/kitap-4.png',
		name: 'Bitmeyen İhanet',
		author: 'Berdal Aral'
	},
	{
		image: '/img/kitaplar/kitap-5.png',
		name: 'Gazze’ye Yolculuk',
		author: 'Sümeyye Ertekin'
	},
	{
		image: '/img/kitaplar/kitap-6.png',
		name: 'Haritada Kan Lekesi',
		author: 'Asım Öz'
	}
];

type VirtualTours = {
	image: string;
	title: string;
};

const VIRTUALTOURS: VirtualTours[] = [
	{
		image: '/img/sanal-geziler/sanal-gezi-1.png',
		title: 'Jerulselam'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-2.png',
		title: 'Church of the Holy Sepulcher'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-3.png',
		title: 'The Stone of Anointing'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-4.png',
		title: 'Over Mount of Olives'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-1.png',
		title: 'Jerulselam'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-2.png',
		title: 'Church of the Holy Sepulcher'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-3.png',
		title: 'The Stone of Anointing'
	},
	{
		image: '/img/sanal-geziler/sanal-gezi-4.png',
		title: 'Over Mount of Olives'
	}
];

type Exhibits = {
	image: string;
	title: string;
	category: string;
};

const EXHIBITS: Exhibits[] = [
	{
		image: '/img/sergiler/sergi-1.png',
		title: 'Kudüs Kapıları',
		category: 'Karma'
	},
	{
		image: '/img/sergiler/sergi-2.png',
		title: 'Beytülhatim',
		category: 'Karma'
	},
	{
		image: '/img/sergiler/sergi-3.png',
		title: 'Mescid-i Aksa ve Eski Şehir',
		category: 'Karma'
	},
	{
		image: '/img/sergiler/sergi-4.png',
		title: 'Kudüs Sokakları',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-5.png',
		title: 'Hayfa',
		category: 'Osman Kantarcı'
	},
	{
		image: '/img/sergiler/sergi-6.png',
		title: 'El Halil',
		category: 'Karma Fotoğraf Sergisi'
	},
	{
		image: '/img/sergiler/sergi-7.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-8.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-9.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-10.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-11.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	},
	{
		image: '/img/sergiler/sergi-12.png',
		title: 'Kayıp Deniz 2015',
		category: 'Sliman Mansour'
	}
];

type Documents = {
	image: string;
	title: string;
	category: string;
};

const DOCS: Documents[] = [
	{
		image: '/img/belgeler/belge-1.png',
		title: 'Magaribe Zaviyesi',
		category: 'Vakıflar Genel Müdürlüğü Arşivi'
	},
	{
		image: '/img/belgeler/belge-2.png',
		title: 'Haseki Sultan Vakfiyeleri',
		category: 'İslam Ansiklopedisi'
	},
	{
		image: '/img/belgeler/belge-3.png',
		title: 'Ebu Medyen Zaviyesi',
		category: 'Mahalle Mektebi'
	},
	{
		image: '/img/belgeler/belge-4.png',
		title: 'Magaribe Mahallesi',
		category: 'Vakıf'
	},
	{
		image: '/img/belgeler/belge-5.png',
		title: 'Faziletli Şehir Kudüs',
		category: 'İslam Ansiklopedisi'
	},
	{
		image: '/img/belgeler/belge-6.png',
		title: 'Magaribe Zaviyesi',
		category: 'İslam Ansiklopedisi'
	}
];

type News = {
	title: string;
	image: string;
	summary: string;
	date: string;
	author: string;
	size: string;
};

const NEWS: News[] = [
	{
		title: 'Son Güncellemelerle Beraber Filistinli Çocuk Tutuklular',
		image: '/img/sergiler/sergi-2.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Tarık Gürhan',
		size: 'small'
	},
	{
		title: 'Uluslararası Hukuka Göre Türkiye’nin Doğu Akdeniz’deki Statüsü',
		image: '/img/sergiler/sergi-3.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Osman Durmaz',
		size: 'small'
	},
	{
		title: 'Türkiye’nin Doğu Akdeniz’deki Politik Statüsü',
		image: '/img/sergiler/sergi-4.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Hakan Sekiz',
		size: 'small'
	},
	{
		title: 'Son Güncellemelerle Beraber Filistinli Çocuk Tutuklular',
		image: '/img/sergiler/sergi-9.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Tarık Gürhan',
		size: 'small'
	},
	{
		title: 'Uluslararası Hukuka Göre Türkiye’nin Doğu Akdeniz’deki Statüsü',
		image: '/img/sergiler/sergi-6.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Osman Durmaz',
		size: 'small'
	},
	{
		title: 'Türkiye’nin Doğu Akdeniz’deki Politik Statüsü',
		image: '/img/sergiler/sergi-7.png',
		summary:
			'Hâlen İsrail nüfusunun yaklaşık %20’sini oluşturmakta, toplamda 1,4 milyon civarındadırlar. Bu, esas olarak güneydeki Nakab çölünde yaşayan Bedevi vatandaşları içerir.',
		date: '12 Mayis 2020',
		author: 'Hakan Sekiz',
		size: 'small'
	}
];

type Videos = {
	image: string;
	title: string;
	category: string;
};

const VIDEOS: Videos[] = [
	{
		image: '/img/sergiler/sergi-1.png',
		title: 'Sürgündeki Sevda Filistin',
		category: 'Ramazan Mut'
	},
	{
		image: '/img/sergiler/sergi-2.png',
		title: 'İşgal altındaki Fislitinde günlük hayat',
		category: 'Rare Footage'
	},
	{
		image: '/img/sergiler/sergi-3.png',
		title: 'Filistin’in Çiçeği Özgür Kudüs Konulu Kısa Film',
		category: 'TRT Haber'
	},
	{
		image: '/img/sergiler/sergi-4.png',
		title: 'Forbidden History Yasak Tarih',
		category: 'Bruce Burgess'
	},
	{
		image: '/img/sergiler/sergi-5.png',
		title: 'Dünyanın En Dindar Topluluğu ‘Harediler’',
		category: 'Ruhi Çenet'
	},
	{
		image: '/img/sergiler/sergi-6.png',
		title: 'Belgesel: Büyük Felaket 1. Bölüm',
		category: 'Ravan Damin'
	},
	{
		image: '/img/sergiler/sergi-1.png',
		title: 'Sürgündeki Sevda Filistin',
		category: 'Ramazan Mut'
	},
	{
		image: '/img/sergiler/sergi-2.png',
		title: 'İşgal altındaki Fislitinde günlük hayat',
		category: 'Rare Footage'
	},
	{
		image: '/img/sergiler/sergi-3.png',
		title: 'Filistin’in Çiçeği Özgür Kudüs Konulu Kısa Film',
		category: 'TRT Haber'
	},
	{
		image: '/img/sergiler/sergi-4.png',
		title: 'Forbidden History Yasak Tarih',
		category: 'Bruce Burgess'
	},
	{
		image: '/img/sergiler/sergi-5.png',
		title: 'Dünyanın En Dindar Topluluğu ‘Harediler’',
		category: 'Ruhi Çenet'
	},
	{
		image: '/img/sergiler/sergi-6.png',
		title: 'Belgesel: Büyük Felaket 1. Bölüm',
		category: 'Ravan Damin'
	}
];

export { BIOS, MAPCAT, MAPS, INFOGRAPHICS, BOOKS, VIRTUALTOURS, EXHIBITS, NEWS, DOCS, VIDEOS };
