import type { SwiperOptions } from 'swiper';
import type { IPageResponse } from './response.type';

type Platform = 'facebook' | 'twitter' | 'url';
type TPlatform = 'facebook' | 'twitter' | 'linkedin' | 'youtube' | 'instagram';

interface ITabCustomEvent extends CustomEvent {
	detail: number;
}

export interface ISection {
	bios;
	articles;
	timeline;
	maps;
	dictionary;
	exhibitions;
	documentaries;
	library;
	infographics;
	documents;
}

export enum ESection {
	bios = 'bios',
	articles = 'articles',
	timeline = 'timeline_posts',
	dictionary = 'dictionary_posts',
	articles = 'artists',
	maps = 'maps',
	library = 'books',
	artworks = 'artworks',
	historicals = 'historical_documents',
	exhibitions = 'exhibitions',
	articles = 'infographics',
	virtualTours = 'virtual_tours',
	documentaries = 'documentaries',
}

export type TAppPage = {
	slug: string;
	key?: SectionType;
	text?: string;
};

export type TSection =
	| 'bios'
	| 'articles'
	| 'timeline_posts'
	| 'dictionary_posts'
	| 'artists'
	| 'maps'
	| 'books'
	| 'artworks'
	| 'historical_documents'
	| 'exhibitions'
	| 'infographics'
	| 'virtual_tours'
	| 'projects'
	| 'documentaries';

export type SectionType =
	| 'home'
	| 'about'
	| 'projects'
	| 'contact'
	| 'bios'
	| 'bioCategories'
	| 'projectCategories'
	| 'timeline'
	| 'dictionary'
	| 'dictionaryLetters'
	| 'exhibitions'
	| 'exhibitionCategories'
	| 'artists'
	| 'artworks'
	| 'documentaries'
	| 'documentaryCategories'
	| 'maps'
	| 'mapCategories'
	| 'library'
	| 'libraryCategories'
	| 'libraryAuthors'
	| 'infographics'
	| 'infographicCategories'
	| 'virtualTours'
	| 'articles'
	| 'articleCategories'
	| 'articleAuthors'
	| 'historicals'
	| 'historicalCategories';

interface IPlatform {
	platform: Platform;
	value: string;
}

type DictionaryLetter = {
	key: string;
	value: string;
};

type DictionaryItem = {
	title: string;
	meaning: string;
	description?: string;
};

type SelectorItem = {
	text: string;
	link: string;
};

interface Biography {
	name?: string;
	birth?: string;
	death?: string;
}

interface Works {
	title: string;
	subtitle?: string;
}

interface BaseCard {
	id: number;
	slug: string;
	title: string;
}

export type BreakpointOptions = {
	[width: number]: SwiperOptions;
	[ratio: string]: SwiperOptions;
};

export interface BiographyProps extends Biography {
	thumbnail?: string;
	slug?: string;
	job?: string;
	title: string;
}

export interface IBiographyContent extends Biography, ISeoContent {
	id?: number;
	arabicName?: string;
	photo?: string;
	works?: Works[];
	job?: string;
	content: string;
	sources?: string;
	alive?: boolean;
}

export type Date = {
	date?: string;
};

export type ScrollDirection = 'top' | 'down';

export interface Exhibition {
	title: string;
	slug: string;
	subtitle?: string;
	thumbnail?: string;
}

export type Artist = Exhibition;

export interface ExhibitionCategory {
	id: number;
	name: string;
	slug: string;
}

export type Work = {
	id: number;
	name: string;
	thumbnail?: string;
	fullImage?: string;
	year?: string;
};

export type DocumentaryCategory = {
	id: number;
	name: string;
	slug: string;
};

export type Documentary = {
	id: number;
	slug: string;
	title: string;
	producer: string;
	thumbnail?: string;
};

export type MapTypes = 'schematic' | 'interactive';

export type TMap = 'schematic' | 'interactive';

interface ISeo {
	title: string;
	description: string;
	image: string;
	ldJson: string;
	type: string;
}

export interface IMap extends BaseCard {
	thumbnail?: string;
	fullImage?: string;
	content?: string;
}

export interface IInfographic {
	id: number;
	slug: string;
	title: string;
	thumbnail?: string;
	fullImage?: string;
	seo?: ISeo;
}

export type RequestField = {
	name: string;
	value: string;
};

type CategoryKeys = keyof CATEGORIES;

export type ListRequest = {
	page?: number;
	categoryId?: number;
	slug?: string;
	otherFields?: string[];
	exclude?: number;
	perPage?: number;
	categoryField?: CATEGORIES[CategoryKeys];
	searchTerm?: string;
	extraParams?: string;
};

export interface IContentList<K> {
	items: K[];
	hasNext: boolean;
	page?: number;
}

export interface ICategory {
	id: number;
	name: string;
	slug?: string;
	count?: number;
	description?: string;
}

export interface ICategoryList {
	items: ICategory[];
}

export type ApiField = {
	LIST: string;
	CONTENT: string;
	CATEGORY: string;
	SEARCH: string;
	FEATURED?: string;
	PAGE?: string;
};

export type ServiceType = keyof ApiField;

export type SectionCategories = {
	[key: string]: string;
};

export interface IVirtualTour {
	id: number;
	slug: string;
	title: string;
	thumbnail?: string;
}

export type ListResult<T> = {
	id: number;
	slug: string;
	title: string;
	content?: {
		rendered: string;
	};
	subtitle?: string;
	thumbnail?: string;
	fullImage?: string;
	meta_data?: T;
	[key: string]: number[];
};

export interface IContent<T> {
	id?: number;
	content: T;
}

export interface IContentAll<T> {
	content: T[];
	total?: number;
}

export interface IOthers<T> {
	items: T[];
}

export interface IBaseContent {
	id: number;
	title: string;
	content: string;
}

export interface ISeoContent extends IBaseContent {
	seo?: ISeo;
}

export interface ILibraryContent extends ISeoContent {
	tags: any;
	author?: string;
	pageCount?: string;
	language?: string;
	distributor?: string;
	dimensions?: string;
	categories?: string;
	translation?: string;
	cover?: string;
	isbn10?: string;
	isbn13?: string;
	fileUrl?: string;
	similarBooks?: ILibraryCard[];
	authorInfo?: string;
	relatedBooks?: ILibraryCard[];
}

type TArticlePostSource = {
	title: string;
};

export interface IArticleContent extends ISeoContent {
	author?: string;
	date?: string;
	summary?: string;
	sources?: TArticlePostSource[];
}

export interface IDocumentaryContent extends ISeoContent {
	views?: number;
	date?: string;
	producer: string;
	director: string;
	videoFile?: string;
	videoEmbed?: string;
}

export interface IList extends ISeoContent {
	slug?: string;
}

type THistoricalDocumentSource = {
	title: string;
	description: string;
};

export interface IHistoricalContent extends ISeoContent {
	slug: string;
	resources?: THistoricalDocumentSource[];
	documents?: ListResult[];
	info: string;
}

type TAboutLogo = {
	image: string;
	link: string;
	title: string;
};

export interface IAboutContent extends ISeoContent {
	summary: string;
	subtitle: string;
	logos?: TAboutLogo[];
}

export interface IContactContent extends ISeoContent {
	address: string;
	mapIframe: string;
	email: string;
}

export interface IStaticMapContent extends ISeoContent {
	thumbnail?: string;
	originalImage?: string;
	categoryId?: number;
}

export interface IDictionaryContent extends ISeoContent {
	subtitle: string;
	categoryId: number;
}

export interface ITimelineContent extends ISeoContent {
	year: string;
	date: string;
	title: string;
	content: string;
	slug: string;
	thumbnail?: string;
	fullImage?: string;
	videoImage?: string;
	videoId?: string;
	dateUnknown?: boolean;
}

export interface ISearch {
	items: ISearchResponse[];
	hasNext: boolean;
}

export interface IPageHelper {
	content?: IPageResponse;
	items?: IPageResponse[];
}

export interface ISearchResponse {
	id: number;
	title: string;
	url: string;
	subtype: TSection;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	'meta-data'?: any;
}

export interface IBaseCard {
	id?: number;
	title: string;
	slug: string;
}

export interface IMainCard extends IBaseCard {
	subtitle?: string;
	thumbnail?: string;
}

export interface IBiographyCard extends IBaseCard {
	thumbnail?: string;
	birth?: string;
	death?: string;
	alive?: boolean;
	subtitle?: string;
}

export interface IExhibitionCard extends IBaseCard {
	subtitle?: string;
	thumbnail?: string;
}

export interface IDocumentaryCard extends IBaseCard {
	subtitle?: string;
	thumbnail?: string;
}

export interface ILibraryCard extends IBaseCard {
	subtitle?: string;
	thumbnail?: string;
}

export interface IInfographicCard extends IBaseCard {
	thumbnail?: string;
}

export interface IVirtualTourCard extends IBaseCard {
	thumbnail?: string;
}

export interface IDictionaryCard extends IBaseCard {
	subtitle?: string;
	description?: string;
}

export interface IHistoricalDocumentCard extends IBaseCard {
	thumbnail?: string;
	subtitle?: string;
}

export interface IArticleCard extends IBaseCard {
	thumbnail?: string;
	author?: string;
	date?: string;
	description?: string;
}

export interface ITimelineCard extends IBaseCard {
	year: string;
	date: string;
	content: string;
	thumbnail?: string;
	fullImage?: string;
	videoImage?: string;
	videoId?: string;
	seo?: ISeo;
}

export interface IArtwork {
	id: number;
	name: string;
	year: string;
	language: string;
	publisher: string;
	thumbnail: string;
	originalImage: string;
	width: number;
	height: number;
}

export enum EPageSection {
	'hakkimizda' = 'about',
	'biyografiler' = 'bios',
}

export type TLink = {
	title: string;
	url: string;
	target: string;
};

export type TStat = {
	name: string;
	description: string;
	value: string;
	icon?: string;
};

export type TScrollerOptions = {
	direction?: 'up' | 'down' | 'left' | 'right';
	duration?: number;
	speed?: number;
	delayBeforeStart?: number;
	duplicated?: boolean;
	gap?: number;
};

export type TOrderer = 'asc' | 'desc';

export type TContentOrderer = {
	[key in SectionType]?: {
		nameAsc?: string;
		nameDesc?: string;
		releasedAsc?: string;
		releasedDesc?: string;
		birthAsc?: string;
		birthDesc?: string;
		authorAsc?: string;
		authorDesc?: string;
		sourceAsc?: string;
		sourceDesc?: string;
	};
};

export type TAction = {
	update?: (params?: any) => void;
	destroy?: (params?: any) => void;
};

export type TOrderType = 'nameAsc' | 'nameDesc' | 'birthAsc' | 'birthDesc';

export interface IMapCategory {
	icon: string;
	title: string;
	description: string;
	id: number;
	link: string;
	disabled?: boolean;
	slug?: string;
}

export interface IProject {
	id: number;
	name: string;
	description: string;
	slug: string;
}

export type THomeItem = {
	section: TSection;
	content?: IMainCard & {
		author?: string;
		birth?: string;
		death?: string;
		date?: string;
		description?: string;
		alive?: boolean;
	};
};

export type SPage = {
	url: URL;
	params: Record<string, string>;
	stuff: App.Stuff;
	status: number;
	error: Error | null;
};

export type TTimelineDate = {
	id: number;
	date: string;
	year: string;
	unknown: boolean;
};

export interface ITimelineDate {
	id: number;
	day: string;
	year: string;
	slug: string;
	unknown: boolean;
}

export interface ITimelineItem extends ITimelineDate {
	thumbnail?: string;
	title: string;
	content?: string;
	date?: string;
}

export interface IProjectCard {
	projectId: number;
	id: number;
	title: string;
	subtitle: string;
	description: string;
	thumbnail: string;
	slug?: string;
	link: string;
}

export interface SvelteMediaTimeRange {
	start: number;
	end: number;
}

export type TContactForm = {
	name?: string;
	email?: string;
	message?: string;
};

export type TService<T> = {
	next: boolean;
	page: number;
	items: T[];
};
