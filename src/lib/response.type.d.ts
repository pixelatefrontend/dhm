import type { TLink, TPlatform, TSection } from './types';

interface BaseResponse {
	id: number;
	slug: string;
	link: string;
}

interface ResponseSeoFields {
	yoast_head: string;
	yoast_head_json: YoastHeadJson;
	_links: Links;
}

export interface ContentResponse extends BaseResponse {
	title: Title;
	content: Content;
}

/** Example Responses */

// export interface Root extends BaseResponse, ResponseSeoFields {
//   count: number
//   description: string
//   name: string
//   taxonomy: string
//   parent: number
//   meta: unknown[]
//   meta_data: boolean
// }

// export interface Root2 extends BaseResponse, ResponseSeoFields {
//   date: string
//   date_gmt: string
//   guid: Guid
//   modified: string
//   modified_gmt: string
//   status: string
//   type: string
//   title: Title
//   content: Content
//   featured_media: number
//   template: string
//   documentary_categories: number[]
//   meta_data: MetaData
// }

interface ResponseBody {
	id: number;
	slug: string;
	status: string;
	type: TSection;
	link: string;
	title: Title;
	content: Content;
	featured_media: number;
	template: string;
	yoast_head: string;
	yoast_head_json: YoastHeadJson;
	_links: Links;
	meta_data: {
		image_details: ImageDetails;
	};
}

export interface IResponseBody {
	id: number;
	slug: string;
	status: string;
	type: TSection;
	link: string;
	title: Title;
	content: Content;
	featured_media: number;
	template: string;
	yoast_head: string;
	yoast_head_json: YoastHeadJson;
	_links: Links;
	meta_data: any;
}

export interface CategoryResponse extends BaseResponse {
	count: number;
	description: string;
	name: string;
	taxonomy: string;
	parent: number;
	meta: unknown[];
	meta_data: boolean;
}

interface Guid {
	rendered: string;
}

interface Title {
	rendered: string;
}

interface Content {
	rendered: string;
	protected: boolean;
}

interface Excerpt {
	rendered: string;
	protected: boolean;
}

interface YoastHeadJson {
	title: string;
	robots: Robots;
	og_locale: string;
	og_type: string;
	og_title: string;
	og_description: string;
	og_url: string;
	og_site_name: string;
	article_modified_time: string;
	og_image: OgImage[];
	twitter_card: string;
	twitter_misc: TwitterMisc;
	schema: Schema;
}

export interface ISeoResponse extends BaseResponse {
	yoast_head_json: YoastHeadJson;
	meta_data: IPageMeta;
}

interface Robots {
	index: string;
	follow: string;
	'max-snippet': string;
	'max-image-preview': string;
	'max-video-preview': string;
}

interface OgImage {
	width: number;
	height: number;
	url: string;
	type: string;
}

interface TwitterMisc {
	'Tahmini okuma süresi': string;
}

interface Schema {
	'@context': string;
	'@graph': Graph[];
}

interface Graph {
	'@type': string;
	'@id': string;
	url?: string;
	name?: string;
	description?: string;
	potentialAction?: PotentialAction[];
	inLanguage?: string;
	contentUrl?: string;
	width?: number;
	height?: number;
	isPartOf?: IsPartOf;
	primaryImageOfPage?: PrimaryImageOfPage;
	datePublished?: string;
	dateModified?: string;
	breadcrumb?: Breadcrumb;
	itemListElement?: ItemListElement[];
}

interface PotentialAction {
	'@type': string;
	target: unknown;
	'query-input'?: string;
}

interface IsPartOf {
	'@id': string;
}

interface PrimaryImageOfPage {
	'@id': string;
}

interface Breadcrumb {
	'@id': string;
}

interface ItemListElement {
	'@type': string;
	position: number;
	name: string;
	item?: string;
}

interface Creation {
	eser_adi: string;
	eser_alternatif_baslik: string;
}

interface ImageDetails {
	thumbnail: Thumbnail;
	full: Full;
}

interface Thumbnail {
	file: string;
	width: number;
	height: number;
	'mime-type': string;
	url: string;
}

interface Full {
	file: string;
	width: number;
	height: number;
	mime_type: string;
	url: string;
}

interface I18nConfig {
	name: Name;
}

interface Name {
	ts: Ts;
}

interface Ts {
	tr: string;
}

interface Links {
	self: Self[];
	collection: Collection[];
	about: About[];
	'wp:featuredmedia': Featuredmedum[];
	'wp:attachment': WpAttachment[];
	'wp:term': WpTerm[];
	curies: Cury[];
}

interface Self {
	href: string;
}

interface Collection {
	href: string;
}

interface About {
	href: string;
}

interface Featuredmedum {
	embeddable: boolean;
	href: string;
}

interface WpAttachment {
	href: string;
}

interface WpTerm {
	taxonomy: string;
	embeddable: boolean;
	href: string;
}

interface Cury {
	name: string;
	href: string;
	templated: boolean;
}

interface ResponseDates {
	date: string;
	date_gmt: string;
	guid: Guid;
	modified: string;
	modified_gmt: string;
}

interface ITimelineMetaVideo {
	kapak_gorseli: IMediaObject;
	video_id: string;
}

export interface Sizes {
	thumbnail: string;
	'thumbnail-width': number;
	'thumbnail-height': number;
	medium: string;
	'medium-width': number;
	'medium-height': number;
	medium_large: string;
	'medium_large-width': number;
	'medium_large-height': number;
	large: string;
	'large-width': number;
	'large-height': number;
	'1536x1536': string;
	'1536x1536-width': number;
	'1536x1536-height': number;
	'2048x2048': string;
	'2048x2048-width': number;
	'2048x2048-height': number;
}

interface IMediaObject {
	ID: number;
	id: number;
	title: string;
	filename: string;
	filesize: number;
	url: string;
	link: string;
	alt: string;
	author: string;
	description: string;
	caption: string;
	name: string;
	status: string;
	uploaded_to: number;
	date: string;
	modified: string;
	menu_order: number;
	mime_type: string;
	type: string;
	subtype: string;
	icon: string;
	width: number;
	height: number;
	sizes: Sizes;
}

interface Post {
	ID: number;
	post_author: string;
	post_date: string;
	post_date_gmt: string;
	post_content: string;
	post_title: string;
	post_excerpt: string;
	post_status: string;
	comment_status: string;
	ping_status: string;
	post_password: string;
	post_name: string;
	to_ping: string;
	pinged: string;
	post_modified: string;
	post_modified_gmt: string;
	post_content_filtered: string;
	post_parent: number;
	guid: string;
	menu_order: number;
	post_type: string;
	post_mime_type: string;
	comment_count: string;
	filter: string;
	post_title_ml: string;
	post_title_langs: PostTitleLangs;
	featured_media: IMedia;
}

interface IArtworkPost extends Post {
	meta_data: {
		eser_sahibi: Post[];
		yapim_yili: string;
		dil: string;
		yayinci: string;
		kisa_aciklama: string;
		dahil_oldugu_sergiler: Post[];
		image_details: IMedia;
		featured_media: IMedia;
	};
}

interface IArtistPost extends Post {
	meta_data: {
		dahil_oldugu_sergiler: Post[];
		eser_sahibi: Post[];
		yapim_yili: string;
		dil: string;
		yayinci: string;
		kisa_aciklama: string;
	};
}

export interface IRelatedBook {
	ID: number;
	post_author: string;
	post_date: string;
	post_date_gmt: string;
	post_content: string;
	post_title: string;
	post_excerpt: string;
	post_status: string;
	comment_status: string;
	ping_status: string;
	post_password: string;
	post_name: string;
	to_ping: string;
	pinged: string;
	post_modified: string;
	post_modified_gmt: string;
	post_content_filtered: string;
	post_parent: number;
	guid: string;
	menu_order: number;
	post_type: string;
	post_mime_type: string;
	comment_count: string;
	filter: string;
	featured_media: IMedia;
	book_categories: Taxonomy[];
	book_authors?: CategoryResponse[];
}

export interface IMedia {
	thumbnail: IMediaProps;
	medium: IMediaProps;
	large: IMediaProps;
	full: IMediaProps;
}

interface IMediaProps {
	file: string;
	width: number;
	height: number;
	'mime-type': string;
	url: string;
}

interface PostTitleLangs {
	tr: boolean;
}

interface Taxonomy {
	term_id: number;
	name: string;
	slug: string;
	term_group: number;
	term_taxonomy_id: number;
	taxonomy: string;
	description: string;
	parent: number;
	count: number;
	filter: string;
	term_order: string;
	i18n_config: I18nConfig;
}

export interface VideoUrl {
	title: string;
	url: string;
	target: string;
}

/**
 * Response Meta Data Types
 */

export interface IBioMeta {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	arapca_isim: string;
	dogum_tarihi: string;
	dogum_tarihi_yil__donem: string;
	olum_tarihi_yil__donem: string;
	olum_tarihi: string;
	eserler: Creation[];
	kaynaklar: string;
	meslek_unvan: string;
	image_details: IMedia;
	bio_categories: Taxonomy[];
	'yasiyor-mu': boolean;
}

export interface IExhibitionMeta {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	sanatci_adi: string;
	secili_eserler: IArtworkPost[];
	image_details: IMedia;
	exhibition_categories: Taxonomy[];
	eserleri?: IArtworkPost[];
}

export interface IArtistMeta {
	eserleri: IArtworkPost[];
	image_details: IMedia;
}

export interface IDocumentaryMeta {
	video_url: VideoUrl;
	video_dosya: boolean;
	yapimci: string;
	yayin_tarihi: string;
	yonetmen: string;
	image_details: IMedia;
	documentary_categories: Taxonomy[];
}

export interface ILibraryMeta {
	yazar: string;
	sayfa_sayisi: string;
	ceviri: string;
	dagitimci: string;
	dil: string;
	isbn_10: string;
	isbn_13: string;
	boyutlar: string;
	yazar_hakkinda: string;
	iliskili_kitaplar: string | Post[];
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	image_details: IMedia;
	book_categories: Taxonomy[];
}

export interface IInfographicMeta {
	image_details: IMedia;
	infographic_categories: Taxonomy[];
}

export interface IVirtualTourMeta {
	gezi_embed_kodu: string;
	image_details: ImageDetails;
}

export interface IDictionaryMeta {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	image_details: ImageDetails;
	orjinal_ifade: string;
	dictionary_letters: Taxonomy[];
}

export interface IHistoricalDocumentMeta {
	belge_kaynagi: string;
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	belgeler: IHistoricalDocument[];
	kaynaklar: IHistoricalDocumentResponseSource[];
	belge_metni: string;
	image_details: IMedia;
}

export interface IArticleMeta {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	yayinlanma_tarihi: string;
	spot_metin: string;
	kisa_aciklama: string;
	kaynaklar: TArticleSourceResponse[];
	image_details: IMedia;
	yazar: string;
	article_authors: Taxonomy[];
	article_categories: Taxonomy[];
}

export interface ITimelineMeta {
	olay_tarihi: string;
	sabitlenmis_icerik: boolean;
	belirsiz: boolean;
	one_cikarilmis_icerik: boolean;
	video_icerik: ITimelineMetaVideo;
	image_details: IMedia;
}

export interface IPageMeta {
	spot_baslik: string;
	aciklama: string;
	ikon: IMediaObject;
	url: string;
	meta_gorsel: string;
	iliskili_post_tipi?: unknown;
	image_details: IMedia;
}

export interface IMapMeta {
	kucuk_gorsel: IMediaObject;
	buyuk_gorsel: IMediaObject;
	spot_metin: string;
	image_details: IMedia;
	map_categories: Taxonomy[];
}

type TArticleSourceResponse = {
	kaynak: string;
};

interface IHistoricalDocumentResponseSource {
	baslik: string;
	aciklama: string;
}

interface IHistoricalDocument {
	belge_adi: string;
	belge_gorseli: IMediaObject;
}

interface PageMetaData {
	icerik_basligi: string;
	giris_metni: string;
	logolar: boolean;
	image_details: ImageDetails;
}

interface StatMetaData {
	ikon: boolean;
	istatistik: string;
	aciklama: string;
	image_details: ImageDetails;
}

interface ProjectMetaData {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	image_details: ImageDetails;
	project_categories: Taxonomy[];
}

interface TimelineMetaData {
	olay_tarihi: string;
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	video_icerik: VideoContent;
	image_details: ImageDetails;
}

interface DictionaryMetaData {
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	image_details: ImageDetails;
	dictionary_letters: Taxonomy[];
}

interface ArtistMetaData {
	eserleri: Post[];
	image_details: ImageDetails;
}

interface ArtworkMetaData {
	eser_sahibi: Post[];
	yapim_yili: string;
	dil: string;
	yayinci: string;
	kisa_aciklama: string;
	dahil_oldugu_sergiler: Post[];
	image_details: ImageDetails;
}

interface DocumentaryMetaData {
	video_url: VideoUrl;
	video_dosya: Media;
	yapimci: string;
	yayin_tarihi: string;
	yonetmen: string;
	image_details: IMedia;
	video_id: string;
	documentary_categories: DefaultCategory[];
}

interface MapMetaData {
	kucuk_gorsel: Media;
	buyuk_gorsel: Media;
	spot_metin: string;
	image_details: ImageDetails;
	map_categories: Taxonomy[];
}

interface BookMetaData {
	yazar: string;
	sayfa_sayisi: string;
	ceviri: string;
	dagitimci: string;
	dil: string;
	isbn_10: string;
	isbn_13: string;
	boyutlar: string;
	yazar_hakkinda: string;
	iliskili_kitaplar: IRelatedBook[];
	sabitlenmis_icerik: boolean;
	one_cikarilmis_icerik: boolean;
	image_details: IMedia;
	book_categories: Taxonomy[];
	kitap_pdf_url: {
		baglanti: VideoUrl;
		pdf_dosyasi: string;
		tip: boolean;
	};
}

interface InfographicMetaData {
	image_details: ImageDetails;
	infographic_categories: Taxonomy[];
}

interface VirtualTourMetaData {
	gezi_embed_kodu: string;
	image_details: ImageDetails;
}

interface IDocument {
	belge_adi: string;
	belge_metni: string;
	belge_gorseli: Media;
}

interface IURLObject {
	title: string;
	url: string;
	target: string;
}

interface IAboutLogo {
	logo_gorseli: IMediaObject;
	logo_baglantisi: IURLObject;
}

export interface IMapCategoryMeta {
	kategori_ikonu: IMediaObject;
}

/**
 * Exported Responses
 */

export interface BioResponse extends ResponseBody, ResponseDates {
	meta_data: IBioMeta;
}

export interface TimelineResponse extends ResponseBody, ResponseDates {
	meta_data: TimelineMetaData;
}

export interface PageResponse extends ResponseBody, ResponseDates {
	excerpt: Excerpt;
	author: number;
	menu_order: number;
	comment_status: string;
	ping_status: string;
	meta: unknown[];
	meta_data: PageMetaData;
}

export interface StatResponse extends ResponseBody {
	parent: string;
	meta_data: StatMetaData;
}

export interface BioCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface ProjectResponse extends ResponseBody, ResponseDates {
	meta_data: ProjectMetaData;
}

export interface ProjectCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface DictionaryResponse extends ResponseBody, ResponseDates {
	dictionary_letters: number[];
}

export interface LetterResponse extends ResponseBody, CategoryResponseFields {}

export interface ExhibitionResponse extends ResponseBody, ResponseDates {
	exhibition_categories: number[];
	meta_data: ExhibitionMetaData;
}

export interface ExhibitionCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface ArtistResponse extends ResponseBody, ResponseDates {
	meta_data: ArtistMetaData;
}

export interface ArtworkResponse extends ResponseBody, ResponseDates {
	meta_data: ArtworkMetaData;
}

export interface DocumentaryResponse extends ContentResponse {
	documentary_categories: number[];
	meta_data: DocumentaryMetaData;
}

export interface DocumentaryCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface MapResponse extends BaseResponse, ContentResponse {
	map_categories: number[];
	meta_data: IStaticMapMeta;
}

export interface MapCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface IBiographyResponse extends ResponseBody, ResponseDates, ResponseSeoFields {
	meta_data: IBioMeta;
}

export interface ILibraryResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	book_categories: number[];
	book_authors: unknown[];
	meta_data: BookMetaData;
}

export interface BookCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface InfographicResponse extends ResponseBody, ResponseDates {
	infographic_categories: number[];
	meta_data: InfographicMetaData;
}

export interface InfographicCategoryResponse extends ResponseBody, CategoryResponseFields {}

export interface VirtualTourResponse extends ResponseBody, ResponseDates, ResponseSeoFields {
	meta_data: VirtualTourMetaData;
}

export interface IArticleResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	article_categories: number[];
	article_authors: number[];
	meta_data: IArticleMeta;
}

export interface IDictionaryResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	dictionary_letters: number[];
	meta_data: IDictionaryMeta;
}

export interface IHistoricalResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	document_categories: number[];
	meta_data: IHistoricalDocumentMeta;
}

export interface IAboutResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: {
		icerik_basligi: string;
		giris_metni: string;
		logolar: IAboutLogo[];
		image_details: ImageDetails;
	};
}

export interface IContactResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: {
		adres: string;
		harita_iframe: string;
		eposta: string;
		image_details: ImageDetails;
	};
}

export interface IDocumentaryResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: DocumentaryMetaData;
}

export interface IMapResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: IMapMeta;
}

export interface IInfographicResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: IInfographicMeta;
	infographic_categories: Taxonomy[];
}

export interface IExhibitionResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: IExhibitionMeta;
	exhibition_categories: number[];
}

export interface IArtistResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: IArtistMeta;
}

export interface ITimelineResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: ITimelineMeta;
}

export interface IPageResponse extends BaseResponse, ContentResponse, ResponseSeoFields {
	meta_data: IPageMeta;
}

export interface IMapCategoryResponse extends BaseResponse, CategoryResponse {
	meta_data: IMapCategoryMeta;
}

export interface ISocialResponse {
	ana_sayfa_aciklama: string;
	ana_sayfa_spot: string;
	site_logosu: IMediaObject;
	sosyal_medya: {
		[key in TPlatform]?: TLink;
	};
}

export interface IStatsResponse {
	slug: string;
	meta_data: {
		ikon: IMediaObject;
		istatistik: string;
		aciklama: string;
	};
}

export interface IProjectsListResponse extends CategoryResponse {
	meta_data: any;
}

export interface ITimelineDateResponse extends BaseResponse {
	meta_data: {
		olay_tarihi: string;
		belirsiz?: boolean;
	};
}

export interface IProjectResponse extends BaseResponse, ContentResponse {
	project_categories: number[];
	meta_data: {
		alt_baslik: string;
		dis_link: string;
		image_details: IMedia;
		one_cikarilmis_icerik: boolean;
		sabitlenmis_icerik: boolean;
		project_categories: Taxonomy[];
		video_url: string;
	};
}
