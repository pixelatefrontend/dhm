import type { TContactForm } from '$lib/types';

const mailBuilder = ({ name, email, message }: TContactForm): string => {
	const mailBody = `
  <html>
    <head>
      <title>${name} - Dijital Hafıza Merkezi İletişim Formu</title>
      <style>
        p { margin: 0; }
        .block { margin-top: 2rem; }
      </style>
    </head>
    <body>
    <div class="block">
      <p><b>Gönderenin Adı Soyadı:</b> ${name}</p>
      <p><b>Gönderenin E-posta Adresi:</b> ${email}</p>
      <div class="block"><p><b>Mesaj:</b></p><p>${message}</p></div>
    </div>
    <div class="block">
      <p>--</p>
      <p>Bu e-posta, Dijital Hafıza Merkezi'nde yer alan iletişim formundan gönderildi.</p>
    </div>
    </body>
    </html>
  `;

	return mailBody;
};

export { mailBuilder };
