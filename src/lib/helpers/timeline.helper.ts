import { timelineStore } from '$stores/timeline.store';
import { get } from 'svelte/store';

const handleSameYear = (index: number): boolean => {
	const { items } = get(timelineStore);
	const previous = items[index - 1];
	const current = items[index];
	if (!previous) return false;
	return previous.year === current.year;
};

export { handleSameYear };
