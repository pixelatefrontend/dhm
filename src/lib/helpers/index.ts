import { apiFields, apiUrl, API_STORE, CATEGORIES, PAGE_URLS, SECTIONS } from '$lib/constants/urls';
import type { Taxonomy } from '$lib/response.type';
import type { ListRequest, SectionType, ServiceType } from '$lib/types';
import { shareStore } from '$stores/share.store';

const stringifyJobs = (p: string, n: string): string => p + ', ' + n;

const servicer = (section: SectionType, type: ServiceType, params?: ListRequest): string => {
	let url: string;
	if (type === 'LIST' || type === 'CONTENT') {
		url = API_STORE[section] + '?_fields=' + apiFields[type];

		if (params && params.otherFields && params.otherFields.length > 0) {
			params.otherFields.map((e) => (url = url + ',meta_data.' + e));
		}

		if (params && params.categoryField) url = url + ',' + params.categoryField;

		if (params && params.categoryId) {
			url = url + '&' + CATEGORIES[section] + '=' + params.categoryId;
		}
	}
	if (type === 'CATEGORY') {
		url = apiUrl + '/' + CATEGORIES[section] + '?_fields=' + apiFields[type];
	}

	if (type === 'FEATURED') {
		return (url = apiUrl + '/post_featured?post_type=' + section + '&_fields=' + apiFields[type]);
	}

	if (type === 'PAGE') {
		return (url = PAGE_URLS[section] + '?_fields=' + apiFields[type]);
	}

	if (type === 'SEARCH') {
		url =
			API_STORE['search'] +
			'?subtype[]=' +
			SECTIONS[section] +
			'&_fields=' +
			apiFields[type] +
			'&search=' +
			params.searchTerm;
	}

	if (params) {
		if (params.perPage) url = url + '&per_page=' + params.perPage;
		else url = url + '&per_page=12';

		if (params.exclude) url = url + '&exclude=' + params.exclude;

		if (params.slug) url = url + '&slug=' + params.slug;

		if (params.page) url = url + '&page=' + params.page;

		if (params.extraParams) url = url + '&' + params.extraParams;
	}
	return url;
};

const stringifyList = (list: Taxonomy[]): string => {
	const items: string[] = list.map((l) => l.name);
	const result = items.reduce((a, i) => a + ', ' + i);
	return result;
};

const removeTags = (str: string): string => {
	if (str === null) throw new Error('Bu bir metin degil.');
	else str = str.toString();
	return str.replace(/(<([^>]+)>)/gi, '');
};

const textCopier = (text: string): void => {
	navigator.clipboard.writeText(text);
};

type TStyleParser = {
	translate3d?: string[];
	scale3d?: string[];
};

function styleParser(str: string): TStyleParser {
	const regex = /(\w+)\((.+?)\)/g;
	const transform: TStyleParser = {};
	let match: string[];

	while ((match = regex.exec(str)))
		transform[match[1]] = transform[match[1]] ? transform[match[1]].concat([match[2]]) : [match[2]];

	return transform;
}

async function fetcher<T>(url: string): Promise<T> {
	try {
		const req = await fetch(url);
		const res: T = await req.json();
		return res;
	} catch (error) {
		return error;
	}
}

const socializer = (url: URL, text: string): void => {
	shareStore.setFacebook(url.toString());
	shareStore.setLinkedin(url.toString());
	shareStore.setTwitter(url.toString(), text);
};

const debouncer = (func: () => void, timeout = 300): (() => void) => {
	let timer: ReturnType<typeof setTimeout>;
	return () => {
		clearTimeout(timer);
		timer = setTimeout(() => {
			func();
		}, timeout);
	};
};

const noscroll = (): void => {
	const scrollPos = parseInt(window.localStorage.getItem('scroll-pos'));
	const scrollTopExists =
		localStorage.getItem('scroll-top') !== null && localStorage.getItem('scroll-top') === '1';
	if (!scrollTopExists) {
		return window.scrollTo(0, scrollPos);
	}
};

export {
	stringifyJobs,
	servicer,
	stringifyList,
	removeTags,
	textCopier,
	styleParser,
	fetcher,
	socializer,
	debouncer,
	noscroll,
};
