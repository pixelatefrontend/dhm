import type { IResponseBody } from '$lib/response.type';
import type { THomeItem } from '$lib/types';
import { removeTags } from '.';

const buildHomeCards = (response: IResponseBody[]): THomeItem[] => {
	const items: THomeItem[] = response.map((i) => {
		const item: THomeItem = {
			section: i.type,
		};

		item.content = {
			title: i.title.rendered,
			slug: i.slug,
			thumbnail: i.meta_data.image_details?.medium?.url || '',
			subtitle: '',
		};

		if (i.type === 'bios') {
			item.content.birth =
				i.meta_data.dogum_tarihi.split(' ').slice(-1).toString() ||
				i.meta_data.dogum_tarihi_yil__donem ||
				'';
			item.content.death =
				i.meta_data.olum_tarihi.split(' ').slice(-1).toString() ||
				i.meta_data.olum_tarihi_yil__donem ||
				'';
			item.content.subtitle =
				i.meta_data.meslek_unvan || i.meta_data.bio_categories?.[0]?.name || '';
			item.content.alive = i.meta_data['yasiyor-mu'];
		}

		if (i.type === 'timeline_posts') {
			item.content.date = i.meta_data.olay_tarihi || '';
		}

		if (i.type === 'dictionary_posts') {
			item.content.subtitle = i.meta_data.orjinal_ifade;
			item.content.description = removeTags(i.content.rendered).slice(0, 250) + '...';
		}

		if (i.type === 'exhibitions') {
			item.content.subtitle =
				i.meta_data.sanatci_adi || i.meta_data.exhibition_categories?.[0]?.name || '';
		}

		if (i.type === 'articles') {
			item.content.author = i.meta_data.article_authors?.[0]?.name || '';
			item.content.date = i.meta_data.yayinlanma_tarihi || '';
		}

		if (i.type === 'historical_documents') {
			item.content.subtitle =
				i.meta_data.belge_kaynagi || i.meta_data.document_categories?.[0]?.name || '';
		}

		if (i.type === 'documentaries') {
			item.content.subtitle =
				i.meta_data.yapimci || i.meta_data.documentary_categories?.[0]?.name || '';
		}

		if (i.type === 'books') {
			item.content.subtitle = i.meta_data.yazar || '';
		}

		return item;
	});

	return items;
};

export { buildHomeCards };
