import type { TContentOrderer, TOrderer } from '$lib/types';

const SEARCH_DELAY = 1000;

const PER_PAGE = 12

const INTRO_FADE_DURATION = 500;

const orderer = (type: TOrderer, field: string, metaKey = ''): string => {
	let query = `order=${type}&orderby=${field}`;
	if (metaKey) query = `${query}&meta_key=${metaKey}`;
	return query;
};

const CONTENT_ORDER_PARAMS: TContentOrderer = {
	bios: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title'),
		birthAsc: orderer('asc', 'meta_value_num', 'dogum_tarihi'),
		birthDesc: orderer('desc', 'meta_value_num', 'dogum_tarihi')
	},
	exhibitions: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title')
	},
	documentaries: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title'),
		releasedAsc: orderer('asc', 'meta_value_num', 'yayin_tarihi'),
		releasedDesc: orderer('desc', 'meta_value_num', 'yayin_tarihi'),
		sourceAsc: orderer('asc', 'meta_value', 'yapimci'),
		sourceDesc: orderer('desc', 'meta_value', 'yapimci')
	},
	maps: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title')
	},
	library: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title')
	},
	infographics: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title')
	},
	historicals: {
		nameAsc: orderer('asc', 'title'),
		nameDesc: orderer('desc', 'title')
	},
  articles: {
    releasedAsc: orderer('asc', 'meta_value', 'yayinlanma_tarihi'),
    releasedDesc: orderer('desc', 'meta_value', 'yayinlanma_tarihi')
  }
};

export { SEARCH_DELAY, CONTENT_ORDER_PARAMS, PER_PAGE, INTRO_FADE_DURATION };
