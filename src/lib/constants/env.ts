const envVars = {
	platform: import.meta.env.VITE_APP_PLATFORM,
};

export { envVars };
