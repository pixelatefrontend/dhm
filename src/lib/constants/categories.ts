import type { API_STORE } from './urls';

type Section =
	| 'bio'
	| 'project'
	| 'exhibition'
	| 'documentary'
	| 'book'
	| 'infographic'
	| 'article';

type Category = {
	section: Section;
	handler: keyof typeof API_STORE;
};

type CategoryItem = {
	id: number;
	name: string;
	slug: string;
};

const CATEGORIES: Category[] = [
	{
		section: 'bio',
		handler: 'bioCategories'
	},
	{
		section: 'project',
		handler: 'projectCategories'
	},
	{
		section: 'exhibition',
		handler: 'exhibitionCategories'
	},
	{
		section: 'documentary',
		handler: 'documentaryCategories'
	},
	{
		section: 'book',
		handler: 'libraryCategories'
	},
	{
		section: 'infographic',
		handler: 'infographicCategories'
	},
	{
		section: 'article',
		handler: 'articleCategories'
	}
];

export { CATEGORIES };
export type { Section, CategoryItem };

