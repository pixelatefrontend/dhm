import type { SelectorItem, TAppPage } from '$lib/types';

const SELECTOR_ITEMS: SelectorItem[] = [
	{
		link: '/',
		text: 'her şey'
	},
	{
		link: '/biyografiler',
		text: 'biyografiler'
	},
	{
		link: '/zaman-tuneli',
		text: 'zaman tüneli'
	},
	{
		link: '/kavramlar-sozlugu',
		text: 'kavramlar sözlüğü'
	},
	{
		link: '/sergiler',
		text: 'sergiler'
	},
	{
		link: '/video-belgeseller',
		text: 'video &amp; belgeseller'
	},
	{
		link: '/haritalar',
		text: 'haritalar'
	},
	{
		link: '/kutuphane',
		text: 'kütüphane'
	},
	{
		link: '/infografikler',
		text: 'infografikler'
	},
	{
		link: '/sanal-gezi',
		text: 'sanal gezi'
	},
	{
		link: '/tarihi-belgeler',
		text: 'tarihi belgeler'
	},
	{
		link: '/kose-yazilari',
		text: 'köşe yazıları'
	}
];

const APP_PAGES: TAppPage[] = [
	{
		slug: '',
		key: 'home'
	},
	{
		slug: 'hakkimizda',
		key: 'about'
	},
	{
		slug: 'iletisim',
		key: 'contact'
	},
	{
		slug: 'kose-yazilari',
		key: 'articles'
	},
	{
		slug: 'tarihi-belgeler',
		key: 'historicals'
	},
	{
		slug: 'sanal-geziler',
		key: 'virtualTours'
	},
	{
		slug: 'infografikler',
		key: 'infographics'
	},
	{
		slug: 'kutuphane',
		key: 'library'
	},
	{
		slug: 'haritalar',
		key: 'maps'
	},
	{
		slug: 'video-belgeseller',
		key: 'documentaries'
	},
	{
		slug: 'sergiler',
		key: 'exhibitions'
	},
	{
		slug: 'kavramlar-sozlugu',
		key: 'dictionary'
	},
	{
		slug: 'zaman-tuneli',
		key: 'timeline'
	},
	{
		slug: 'projeler',
		key: 'projects'
	},
	{
		slug: 'anasayfa',
		key: 'home'
	},
	{
		slug: 'biyografiler',
		key: 'bios'
	}
];

export { SELECTOR_ITEMS, APP_PAGES };
