import type { ApiField, SectionCategories, SectionType, TSection } from '$lib/types';

const baseUrl = 'https://cms.dijitalhafiza.com';

const localApiUrls = {
	tr: `${baseUrl}/wp-json/wp/v2`,
	en: `${baseUrl}/en/wp-json/wp/v2`,
};

const apiUrl = import.meta.env.VITE_APP_LOCALE === 'en' ? localApiUrls.en : localApiUrls.tr;

const apiFields: ApiField = {
	LIST: 'id,title,slug,meta_data,content',
	CONTENT: 'id,title,slug,content,meta_data,yoast_head_json',
	CATEGORY: 'id,name,slug,count,description',
	SEARCH: 'id,title,url,subtype,meta-data',
	FEATURED: 'id,title,slug,meta_data',
	PAGE: 'id,title,slug,meta_data,yoast_head_json',
};

const API_STORE = {
	home: `${apiUrl}/homefeed`,
	homeMobile: `${apiUrl}/homefeedmobile?_fields=id,content,title,slug,type,meta_data`,
	pages: `${apiUrl}/pages`,
	bios: `${apiUrl}/bios`,
	bioCategories: `${apiUrl}/bio_categories`,
	projects: `${apiUrl}/projects`,
	projectCategories: `${apiUrl}/project_categories`,
	timeline: `${apiUrl}/timeline_posts_ordered`,
	timelineItems: `${apiUrl}/timeline_posts_ordered?_fields=id,slug,title,content.rendered,meta_data.image_details.medium.url,meta_data.olay_tarihi,meta_data.belirsiz`,
	timelinePosts: `${apiUrl}/timeline_posts`,
	dictionary: `${apiUrl}/dictionary_posts?_fields=id,title,slug,content,dictionary_letters,meta_data,yoast_head_json`,
	dictionaryLetters: `${apiUrl}/dictionary_letters`,
	exhibitions: `${apiUrl}/exhibitions?_fields=id,slug,title,meta_data`,
	exhibitionCategories: `${apiUrl}/exhibition_categories?_fields=id,name,count,slug,description`,
	artists: `${apiUrl}/artists?_fields=id,slug,title,meta_data.eserleri,meta_data.image_details.medium`,
	artworks: `${apiUrl}/artworks`,
	documentaries: `${apiUrl}/documentaries`,
	documentaryCategories: `${apiUrl}/documentary_categories`,
	maps: `${apiUrl}/maps`,
	mapCategories: `${apiUrl}/map_categories`,
	library: `${apiUrl}/books`,
	libraryCategories: `${apiUrl}/book_categories`,
	libraryAuthors: `${apiUrl}/book_authors`,
	infographics: `${apiUrl}/infographics`,
	infographicCategories: `${apiUrl}/infographic_categories`,
	virtualTours: `${apiUrl}/virtual_tours`,
	articles: `${apiUrl}/articles`,
	articleCategories: `${apiUrl}/article_categories`,
	articleAuthors: `${apiUrl}/article_authors`,
	historicals: `${apiUrl}/historical_documents`,
	historicalCategories: `${apiUrl}/document_categories`,
	socials: `${apiUrl}/acfoptions`,
	stats: `${apiUrl}/stats`,
	about: `${apiUrl}/pages/22`,
	contact: `${apiUrl}/pages/44`,
	search: `${apiUrl}/search/`,
	featured: `${apiUrl}/post_featured?post_type=`,
};

const API_PARTS: TSection[] = [
	'bios',
	'dictionary_posts',
	'exhibitions',
	'timeline_posts',
	'documentaries',
	'historical_documents',
];

type TPageUrl = {
	[key in SectionType]?: string;
};

const PAGE_URLS: TPageUrl = {
	bios: `${apiUrl}/pages/365`,
	timeline: `${apiUrl}/pages/369`,
	dictionary: `${apiUrl}/pages/371`,
	exhibitions: `${apiUrl}/pages/361`,
	documentaries: `${apiUrl}/pages/374`,
	maps: `${apiUrl}/pages/376`,
	library: `${apiUrl}/pages/378`,
	infographics: `${apiUrl}/pages/380`,
	virtualTours: `${apiUrl}/pages/382`,
	historicals: `${apiUrl}/pages/384`,
	articles: `${apiUrl}/pages/386`,
	home: `${apiUrl}/pages/9741`,
	projects: `${apiUrl}/pages/367`,
	contact: `${apiUrl}/pages/44`,
	about: `${apiUrl}/pages/22`,
};

const CATEGORIES: SectionCategories = {
	bios: 'bio_categories',
	projects: 'project_categories',
	exhibitions: 'exhibition_categories',
	documentaries: 'documentary_categories',
	maps: 'map_categories',
	library: 'book_categories',
	infographics: 'infographic_categories',
	articles: 'article_categories',
	dictionary: 'dictionary_letters',
	historicals: 'document_categories',
};

const SECTIONS: SectionCategories = {
	bios: 'bios',
	exhibitions: 'exhibitions',
	documentaries: 'documentaries',
	maps: 'maps',
	library: 'books',
	infographics: 'infographics',
	virtualTours: 'virtual_tours',
	articles: 'articles',
	dictionary: 'dictionary_posts',
	historicals: 'historical_documents',
};

const APP_ROUTES = {
	home: '',
	bios: 'biyografiler',
	timeline: 'zaman-tuneli',
	dictionary: 'kavramlar-sozlugu',
	exhibitions: 'sergiler',
	documentaries: 'video-belgeseller',
	maps: 'haritalar',
	library: 'kutuphane',
	infographics: 'infografikler',
	virtualTours: 'sanal-gezi',
	historicals: 'tarihi-belgeler',
	articles: 'kose-yazilari',
};

const APP_SECTIONS = {
	'': 'home',
	biyografiler: 'bios',
	'zaman-tuneli': 'timeline',
	'kavramlar-sozlugu': 'dictionary',
	sergiler: 'exhibitions',
	'video-belgeseller': 'documentaries',
	haritalar: 'maps',
	kutuphane: 'library',
	infografikler: 'infographics',
	'sanal-gezi': 'virtualTours',
	'tarihi-belgeler': 'historicals',
	'kose-yazilari': 'articles',
};

export {
	apiUrl,
	API_STORE,
	apiFields,
	CATEGORIES,
	PAGE_URLS,
	APP_ROUTES,
	APP_SECTIONS,
	SECTIONS,
	API_PARTS,
};
