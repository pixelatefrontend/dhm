import type { IPlatform } from "$lib/types";

const PLATFORMS: IPlatform[] = [
	{
		platform: 'facebook',
		value: 'f'
	},
	{
		platform: 'twitter',
		value: 't'
	},
	{
		platform: 'url',
		value: 'u'
	}
];

export { PLATFORMS };
