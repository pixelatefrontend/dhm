import { API_STORE } from '$lib/constants/urls';
import { removeTags } from '$lib/helpers';
import { buildHomeCards } from '$lib/helpers/home.helper';
import type { IResponseBody } from '$lib/response.type';
import type { THomeItem } from '$lib/types';

type TPromise = {
	items: THomeItem[];
};

type TMobilePromise = {
	items: THomeItem[];
	next: boolean;
	page: number;
};

interface IProps {
	fetch(info: RequestInfo, init?: RequestInit): Promise<Response>;
}

interface IMobileProps extends IProps {
	page: number;
}

const getHomeCards = async ({ fetch }: IProps): Promise<TPromise> => {
	let items: THomeItem[];

	try {
		const request = await fetch(API_STORE.home);
		const response: IResponseBody[] = await request.json();

		if (request.status === 400) {
			throw new Error('Bu liste hatalı.');
		}

		items = buildHomeCards(response);
	} catch (error) {
		throw new Error(`Ana sayfa datasi cekilirken hata oldu: \n ${error}`);
	}

	return { items };
};

const getHomeCardsMobile = async ({ fetch, page }: IMobileProps): Promise<TMobilePromise> => {
	let items: THomeItem[];

	try {
		const request = await fetch(`${API_STORE.homeMobile}&posts_per_page=50&page=${page}`);
		const response: IResponseBody[] = await request.json();

		const total = parseInt(request.headers.get('x-wp-totalpages'), 10);
		const next = page < total;

		if (request.status === 400) {
			throw new Error('Bu liste hatalı.');
		}

		items = buildHomeCards(response);

		return { items, next, page };
	} catch (error) {
		throw new Error(`Ana sayfa datasi cekilirken hata oldu: \n ${error}`);
	}
};

export { getHomeCards, getHomeCardsMobile };
