import { PER_PAGE } from '$lib/constants';
import { API_STORE } from '$lib/constants/urls';
import type { IArtistResponse, IExhibitionResponse } from '$lib/response.type';
import type { ICategory, IExhibitionCard, TService } from '$lib/types';

type TProps = {
	categoryId: number;
	page?: number;
};

const getExhibitionCards = async ({
	categoryId,
	page = 1,
}: TProps): Promise<TService<IExhibitionCard>> => {
	try {
		const URL =
			categoryId === -1
				? `${API_STORE.artists}&page=${page}&per_page=${PER_PAGE}`
				: `${API_STORE.exhibitions}&exhibition_categories=${categoryId}&page=${page}&per_page=${PER_PAGE}`;

		const req = await fetch(URL);
		const res: IExhibitionResponse[] = await req.json();

		const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
		const next = page < total;

		const items = res.map((r) => {
			const result: IExhibitionCard = {
				id: r.id,
				title: r.title.rendered,
				slug: r.slug,
				thumbnail: r.meta_data.image_details.medium.url || '',
			};

			result.subtitle =
				categoryId === -1
					? r.meta_data.eserleri.length + ' Eser'
					: r.meta_data.sanatci_adi || r.meta_data.exhibition_categories?.[0].name || '';

			return result;
		});

		if (req.status === 400) {
			throw new Error('Bu liste hatalı.');
		}

		return { page, next, items };
	} catch (error) {
		throw new Error(`Sergileri cekerken hata oldu: \n${error}`);
	}
};

const getArtistCards = async (page: number): Promise<TService<IExhibitionCard>> => {
	try {
		const req = await fetch(`${API_STORE.artists}&page=${page}&per_page=${PER_PAGE}`);
		const res: IArtistResponse[] = await req.json();

		const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
		const next = page < total;

		const items: IExhibitionCard[] = res.map((r) => {
			return {
				title: r.title.rendered,
				slug: r.slug,
				thumbnail: r.meta_data.image_details.medium.url || '',
				subtitle: r.meta_data.eserleri.length + ' Eser',
			};
		});

		if (req.status === 400) {
			throw new Error('Bu liste hatalı.');
		}

		return { page, next, items };
	} catch (error) {
		throw new Error(`Sergileri cekerken hata oldu: \n${error}`);
	}
};

const getExhibitionCategories = async (): Promise<ICategory[]> => {
	try {
		const req = await fetch(API_STORE.exhibitionCategories);
		const res = await req.json();
		const items = [...res];
		return items;
	} catch (error) {
		throw new Error(`Sergi kategorileri cekilirken hata: ${error}`);
	}
};

export { getExhibitionCards, getArtistCards, getExhibitionCategories };
