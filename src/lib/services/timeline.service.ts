import { API_STORE } from '$lib/constants/urls';
import type { ITimelineDateResponse, ITimelineResponse } from '$lib/response.type';
import type { ITimelineContent, ITimelineDate, ITimelineItem, TService } from '$lib/types';

type TPromise = {
	total: number;
	next: boolean;
	items: ITimelineContent[];
};

const getTimelineEvents = async (page = 1): Promise<TPromise> => {
	const req = await fetch(`${API_STORE.timeline}?posts_per_page=40&page=${page}`);
	const res: ITimelineResponse[] = await req.json();

	const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
	const next = page < total;

	const items: ITimelineContent[] = res.map((i) => {
		return {
			id: i.id,
			title: i.title.rendered,
			slug: i.slug,
			year: i.meta_data.olay_tarihi?.split(' ').slice(-1).toString(),
			date: i.meta_data.olay_tarihi,
			content: i.content.rendered,
			thumbnail: i.meta_data.image_details.medium?.url || '',
			fullImage: i.meta_data.image_details.full?.url || '',
			dateUnknown: i.meta_data.belirsiz,
			seo: {
				title: i.yoast_head_json.title,
				description: i.yoast_head_json.og_description,
				image: i.yoast_head_json.og_image?.[0]?.url,
				type: i.yoast_head_json.og_type,
				ldJson: JSON.stringify(i.yoast_head_json.schema),
			},
		};
	});

	if (req.status === 400) {
		throw new Error('Bu liste hatalı.');
	}

	return { total, next, items };
};

const getTimelineDates = async (): Promise<ITimelineDate[]> => {
	try {
		const req = await fetch(
			`${API_STORE.timeline}?_fields=id,slug,meta_data.olay_tarihi,meta_data.belirsiz&posts_per_page=-1`
		);
		const res: ITimelineDateResponse[] = await req.json();

		const items: ITimelineDate[] = res.map((i) => {
			return {
				id: i.id,
				slug: i.slug,
				day: i.meta_data.olay_tarihi.substring(0, 6),
				year: i.meta_data.olay_tarihi.split(' ').slice(-1).toString(),
				unknown: i.meta_data.belirsiz,
			};
		});

		return items;
	} catch (error) {
		throw new Error(`Zaman tuneli cekilirken hata: ${error}`);
	}
};

type TProps = {
	fetch(info: RequestInfo, init?: RequestInit): Promise<Response>;
	slug: string;
};

const getTimelineContent = async ({ fetch, slug }: TProps): Promise<ITimelineContent> => {
	try {
		const req = await fetch(`${API_STORE.timelinePosts}?slug=${slug}`);
		const res: ITimelineResponse[] = await req.json();
		const event: ITimelineContent = {
			id: res[0].id,
			title: res[0].title.rendered,
			slug: res[0].slug,
			content: res[0].content.rendered,
			date: res[0].meta_data.olay_tarihi,
			year: res[0].meta_data.olay_tarihi.split(' ').slice(-1).toString(),
			fullImage: res[0].meta_data.image_details.full.url || '',
			thumbnail: res[0].meta_data.image_details.medium.url || '',
			dateUnknown: res[0].meta_data.belirsiz,
			seo: {
				title: res[0].yoast_head_json.title,
				description: res[0].yoast_head_json.og_description,
				image: res[0].yoast_head_json.og_image?.[0]?.url,
				type: res[0].yoast_head_json.og_type,
				ldJson: JSON.stringify(res[0].yoast_head_json.schema),

			},
		};

		return event;
	} catch (error) {
		throw new Error(`Zaman tuneli icerigi cekilirken hata: ${error}`);
	}
};

const getTimelineItems = async (page: number): Promise<TService<ITimelineItem>> => {
	try {
		const req = await fetch(`${API_STORE.timelineItems}&page=${page}`);
		const res: ITimelineResponse[] = await req.json();

		const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
		const next = page < total;

		const items: ITimelineItem[] = res.map((i) => {
			return {
				id: i.id,
				title: i.title.rendered,
				slug: i.slug,
				year: i.meta_data.olay_tarihi?.split(' ').slice(-1).toString(),
				day: i.meta_data.olay_tarihi.substring(0, 6),
				date: i.meta_data.olay_tarihi,
				content: i.content.rendered,
				thumbnail: i.meta_data.image_details.medium?.url || '',
				unknown: i.meta_data.belirsiz,
			};
		});

		if (req.status === 400) {
			throw new Error('Bu liste hatalı.');
		}
		return { items, next, page };
	} catch (error) {
		throw new Error(`Zaman tuneli olaylari cekilirken hata oldu: ${error}`);
	}
};

export { getTimelineEvents, getTimelineDates, getTimelineContent, getTimelineItems };
