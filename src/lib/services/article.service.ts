import { API_STORE } from '$lib/constants/urls';
import type { IArticleResponse } from '$lib/response.type';
import type { IArticleCard } from '$lib/types';

const getFeaturedArticles = async (): Promise<IArticleCard[]> => {
	try {
		const req = await fetch(`${API_STORE.featured}articles`);
		const res: IArticleResponse[] = await req.json();

		let items: IArticleCard[] = null;

		if (req.status === 404) {
			items = [];
		} else {
			items = res.map((i) => {
				return {
					id: i.id,
					slug: i.slug,
					title: i.title.rendered,
					thumbnail: i.meta_data.image_details.medium.url || '',
					author: i.meta_data.article_authors?.[0]?.name || '',
					date: i.meta_data.yayinlanma_tarihi || '',
					description: i.meta_data.spot_metin || '',
				};
			});
		}

		return items;
	} catch (error) {
		throw new Error(`One cikan kose yazilarini cekerken hata oldu: ${error}`);
	}
};

export { getFeaturedArticles };
