import { API_STORE } from '$lib/constants/urls';
import { removeTags } from '$lib/helpers';
import type { IDictionaryResponse } from '$lib/response.type';
import type { IDictionaryCard } from '$lib/types';

type TPromise = {
	total: number;
	next: boolean;
	items: IDictionaryCard[];
};

const getLetterCards = async (id: number, page = 1): Promise<TPromise> => {
	const req = await fetch(`${API_STORE.dictionary}&dictionary_letters=${id}&page=${page}`);
	const res: IDictionaryResponse[] = await req.json();

	const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
	const next = page < total;

	const items: IDictionaryCard[] = res.map((r) => {
		return {
			title: r.title.rendered,
			subtitle: r.meta_data.orjinal_ifade || '',
			slug: r.slug,
			description:
				r.content.rendered.length > 0 ? removeTags(r.content.rendered).slice(0, 250) + '...' : ''
		};
	});

	if (req.status === 400) {
		throw new Error('Bu liste hatalı.');
	}

	return { total, next, items };
};

export { getLetterCards };
