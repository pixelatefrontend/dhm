import { PER_PAGE } from '$lib/constants';
import { API_STORE } from '$lib/constants/urls';
import type { IProjectResponse, IProjectsListResponse } from '$lib/response.type';
import type { IProject, IProjectCard } from '$lib/types';

type TPromise = {
	total: number;
	next: boolean;
	items: IProject[];
};

const getProjectsList = async (page = 1): Promise<TPromise> => {
	const req = await fetch(`${API_STORE.projectCategories}?page=${page}&per_page=${PER_PAGE}`);
	const res: IProjectsListResponse[] = await req.json();

	const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
	const next = page < total;

	const items: IProject[] = res.map((r) => {
		return { ...r };
	});

	if (req.status === 400) {
		throw new Error('Bu liste hatalı.');
	}

	return { total, next, items };
};

const getProjectsByCategory = async (id: number): Promise<IProjectCard[]> => {
	try {
		const req = await fetch(`${API_STORE.projects}?project_categories=${id}`);
		const res: IProjectResponse[] = await req.json();

		let items: IProjectCard[] = [];

		if (res.length === 0) {
			return items;
		}

		items = res.map((r) => {
			return {
				id: r.id,
				projectId: id,
				title: r.title.rendered,
				subtitle: r.meta_data.alt_baslik,
				description: r.content.rendered,
				slug: r.slug,
				thumbnail: r.meta_data.image_details.medium.url || '',
				link: r.meta_data.dis_link || r.meta_data.video_url,
			};
		});

		return items;
	} catch (error) {
		throw new Error(`Projeler cekilirken hata: ${error}`);
	}
};

export { getProjectsList, getProjectsByCategory };
