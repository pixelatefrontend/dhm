import { PAGE_URLS } from '$lib/constants/urls';
import type { ISeoResponse } from '$lib/response.type';
import type { ISeo, SectionType } from '$lib/types';

const getSeoData = async (section: SectionType): Promise<ISeo> => {
	try {
		const req = await fetch(`${PAGE_URLS[section]}`);

		const res: ISeoResponse = await req.json();

		const result = {
			title: res.meta_data.spot_baslik + " | Dijital Hafıza Merkezi",
			description: res.meta_data.aciklama,
			image: res.meta_data.meta_gorsel,
			type: res.yoast_head_json.og_type,
			ldJson: JSON.stringify(res.yoast_head_json.schema),
		};

		return result;
	} catch (error) {
		throw new Error(`Seo data cekilirken hata: \n${error}`);
	}
};

export { getSeoData };
