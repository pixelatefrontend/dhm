import { PER_PAGE } from '$lib/constants';
import { API_STORE } from '$lib/constants/urls';
import type { IDictionaryResponse } from '$lib/response.type';
import type { IDictionaryContent } from '$lib/types';

const getDictionaryContent = async (slug: string): Promise<IDictionaryContent> => {
	try {
		const req = await fetch(
			`${API_STORE.dictionary}&per_page=${PER_PAGE}&slug=${slug}`
		);

		const res: IDictionaryResponse[] = await req.json();

		const content: IDictionaryContent = {
			id: res[0].id,
			title: res[0].title.rendered,
			subtitle: res[0].meta_data.orjinal_ifade || '',
			content: res[0].content.rendered,
			categoryId: res[0].dictionary_letters[0],
			seo: {
				title: res[0].yoast_head_json.title,
				description: res[0].yoast_head_json.og_description,
				image: res[0].yoast_head_json.og_image?.[0]?.url,
				type: res[0].yoast_head_json.og_type,
				ldJson: JSON.stringify(res[0].yoast_head_json.schema),
			},
		};

		return content;
	} catch (error) {
		throw new Error(`Sozluk icerigi cekilirken hata: \n ${error}`);
	}
};

export { getDictionaryContent };
