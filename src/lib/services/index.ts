import { API_STORE } from '$lib/constants/urls';
import { servicer } from '$lib/helpers';
import type {
	CategoryResponse,
	ContentResponse,
	IPageResponse,
	ResponseBody
} from '$lib/response.type';
import type {
	ICategory,
	ICategoryList,
	IContent,
	IContentAll,
	IContentList,
	IOthers,
	IPageHelper,
	ISearch,
	ISearchResponse,
	ListRequest,
	ListResult,
	SectionType
} from '$lib/types';

async function getList<K>(
	section: SectionType,
	params?: ListRequest
): Promise<IContentList<ListResult<K>>> {
	const URL = servicer(section, 'LIST', params);
	try {
		const request = await fetch(URL);
		const response: ResponseBody[] = await request.json();
		const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== params.page;

		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		const items: ListResult<K>[] = response.map((e) => ({
			...e,
			id: e.id,
			title: e.title.rendered,
			slug: e.slug
		}));

		return { items, hasNext };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getCategories(section: SectionType): Promise<ICategoryList> {
	const URL = servicer(section, 'CATEGORY', { perPage: 100 });
	try {
		const request = await fetch(URL);
		const response: CategoryResponse[] = await request.json();

		let items: ICategory[] = response.map((e) => {
			return {
				id: e.id,
				name: e.name,
				slug: e.slug,
				count: e.count,
				description: e.description
			};
		});
		items = items.filter((e) => e.count !== 0);
		return { items };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getContent<T extends ContentResponse>(
	section: SectionType,
	slug?: string
): Promise<IContent<T>> {
	try {
		const URL = servicer(section, 'CONTENT', { slug });
		const request = await fetch(URL);
		const response: T[] = await request.json();
		const id = response[0].id;
		const content: T = response[0];
		return { content, id };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getPage<T extends ContentResponse>(section: SectionType): Promise<IContent<T>> {
	try {
		const URL = servicer(section, 'CONTENT');
		const request = await fetch(URL);
		const content: T = await request.json();
		return { content };
	} catch (error) {
		throw Error(`Cannot get page content: \n ${error}`);
	}
}

async function getContentAll<T extends ContentResponse>(
	section: SectionType,
	categoryField?: string
): Promise<IContentAll<T>> {
	try {
		const URL = servicer(section, 'CONTENT', { categoryField, perPage: 100 });
		const request = await fetch(URL);
		const total = parseInt(request.headers.get('x-wp-totalpages'), 10)
		const content: T[] = await request.json();
		return { content, total };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getFeaturedItems<T extends ContentResponse>(
	section: SectionType
): Promise<IContentAll<T>> {
	try {
		const URL = servicer(section, 'FEATURED');
		const request = await fetch(URL);
		const content: T[] = await request.json();
		return { content };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getOthers<T extends ContentResponse>(
	section: SectionType,
	id: number
): Promise<IOthers<T>> {
	try {
		const URL = servicer(section, 'CONTENT', { exclude: id, perPage: 3, extraParams: 'orderby=rand' });
		const request = await fetch(URL);
		const items: T[] = await request.json();
		return { items };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

type SearchParams = {
	section: SectionType;
	term: string;
	page?: number;
};

async function getSearches(params: SearchParams): Promise<ISearch> {
	try {
		const URL = servicer(params.section, 'SEARCH', { page: params.page, searchTerm: params.term });
		const request = await fetch(URL);
		const items: ISearchResponse[] = await request.json();
		const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== params.page;
		return { items, hasNext };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getPageSpots(section: SectionType): Promise<IPageHelper> {
	try {
		const URL = servicer(section, 'PAGE');
		const request = await fetch(URL);
		const content: IPageResponse = await request.json();
		return { content };
	} catch (error) {
		throw Error(`Cannot get list: \n ${error}`);
	}
}

async function getPages(): Promise<IPageHelper> {
	try {
		const URL = `${API_STORE.pages}?per_page=100`
		const request = await fetch(URL);
		const items: IPageResponse[] = await request.json();
		return { items };
	} catch (error) {
		throw Error(`Error while getting pages.\n URL: ${URL} \n ${error}`);
	}
}

export {
	getList,
	getCategories,
	getContent,
	getContentAll,
	getOthers,
	getPage,
	getSearches,
	getFeaturedItems,
	getPageSpots,
	getPages
};
