import { PER_PAGE } from '$lib/constants';
import { API_STORE } from '$lib/constants/urls';
import type { IMapResponse } from '$lib/response.type';
import type { IMap } from '$lib/types';

type TPromise = {
	total: number;
	next: boolean;
	items: IMap[];
};

const getMapCards = async (id: number, page = 1): Promise<TPromise> => {
	const req = await fetch(
		`${API_STORE.maps}?map_categories=${id}&page=${page}&per_page=${PER_PAGE}`
	);
	const res: IMapResponse[] = await req.json();

	const total = parseInt(req.headers.get('x-wp-totalpages'), 10);
	const next = page < total;

	const items: IMap[] = res.map((r) => {
		return {
			id: r.id,
			title: r.title.rendered,
			slug: r.slug,
			thumbnail: r.meta_data.image_details.medium.url || ''
		};
	});

	if (req.status === 400) {
		throw new Error('Bu liste hatalı.');
	}

	return { total, next, items };
};

export { getMapCards };
