<script>
	import { pageStore } from '$stores/page.store';

	import { createEventDispatcher, onDestroy } from 'svelte';
	import { fade } from 'svelte/transition';

	const dispatch = createEventDispatcher();
	const close = () => pageStore.setVisibleShareBox(false);

	let modal;

	const handle_keydown = (e) => {
		if (e.key === 'Escape') {
			close();
			return;
		}

		if (e.key === 'Tab') {
			// trap focus
			const nodes = modal.querySelectorAll('*');
			const tabbable = Array.from(nodes).filter((n) => n.tabIndex >= 0);

			let index = tabbable.indexOf(document.activeElement);
			if (index === -1 && e.shiftKey) index = 0;

			index += tabbable.length + (e.shiftKey ? -1 : 1);
			index %= tabbable.length;

			tabbable[index].focus();
			e.preventDefault();
		}
	};

	const previously_focused = typeof document !== 'undefined' && document.activeElement;

	if (previously_focused) {
		onDestroy(() => {
			// @ts-ignore
			previously_focused.focus();
		});
	}
</script>

<svelte:window on:keydown={handle_keydown} />

<div class="modal-background" on:click={close} transition:fade />

<div class="modal" role="dialog" aria-modal="true" bind:this={modal} transition:fade>
	<slot />
	<!-- svelte-ignore a11y-autofocus -->
	<!-- <button autofocus on:click={close}>close modal</button> -->
</div>

<style>
	.modal-background {
		position: fixed;
		background: rgba(0, 0, 0, 0.5);
		z-index: 150;
		width: 100%;
		height: 100%;
		min-height: 100vh;
		top: 0;
		left: 0;
	}

	.modal {
		position: fixed;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		z-index: 151;
	}

	button {
		display: block;
	}
</style>
