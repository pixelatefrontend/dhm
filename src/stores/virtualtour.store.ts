import type { IVirtualTourMeta } from '$lib/response.type';
import { getList } from '$lib/services';
import type { IVirtualTourCard } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	items: IVirtualTourCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

function handleVirtualTourStore() {
	const data: TStore = {
		items: [],
		hasNext: true,
		isLoaded: false,
		page: 1
	};

	const virtualTours = writable(data);

	const setList = async (page: number) => {
		const { items, hasNext } = await getList<IVirtualTourMeta>('virtualTours', {
			page
		});
		virtualTours.update((v) => {
			const formattedItems: IVirtualTourCard[] = items.map((i) => {
				const item: IVirtualTourCard = {
					title: i.title,
					slug: i.slug
				};

				item.thumbnail = i.meta_data.image_details.full.url || '';

				return item;
			});

			return { ...v, items: [...v.items, ...formattedItems], hasNext, isLoaded: true };
		});
	};

	const setPage = (page: number) => {
		virtualTours.update((v) => ({ ...v, page }));
	};

	return {
		subscribe: virtualTours.subscribe,
		setList,
		setPage
	};
}

const virtualToursStore = handleVirtualTourStore();

export { virtualToursStore };
