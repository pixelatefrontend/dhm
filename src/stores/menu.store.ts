import { writable } from 'svelte/store';

function menuStore() {
	const data = {
		active: false,
		persist: false
	};

	const menu = writable(data);

	const setActive = (status: boolean) => menu.update((v) => ({ ...v, active: status }));

	const setPersist = (status: boolean) => menu.update((v) => ({ ...v, persist: status }));

	return {
		subscribe: menu.subscribe,
		setActive,
		setPersist
	};
}

const menu = menuStore();

export { menu };
