import type { IArticleMeta, IArticleResponse } from '$lib/response.type';
import { getCategories, getFeaturedItems, getList } from '$lib/services';
import type { IArticleCard, ICategory } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IArticleCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	activeCategoryId: number;
	categories: ICategory[];
	featuredItems: IArticleCard[];
	featuredItemsLoaded: boolean;
};

function handleArticleStore() {
	const data: TStore = {
		categories: [
			{
				id: 0,
				name: 'Hepsi',
			},
		],
		items: [],
		activeCategoryId: 0,
		featuredItems: [],
		featuredItemsLoaded: false,
	};

	const store = writable(data);

	const _articles = get(store);

	const setCategories = async () => {
		if (_articles.categories.length < 2) {
			const { items } = await getCategories('articles');
			store.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1,
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1,
				};
				const result = { ...v, categories: [...v.categories, ...items] };
				return result;
			});
		}
	};

	const setPage = (page: number, categoryId: number) => {
		store.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		store.update((v) => ({ ...v, activeCategoryId: id }));
	};

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<IArticleMeta>('articles', {
			page,
			categoryId,
		});
		store.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: IArticleCard = {
					title: i.title,
					slug: i.slug,
				};

				item.thumbnail = i.meta_data.image_details.medium.url || '';
				item.author = i.meta_data.article_authors?.[0]?.name || '';
				item.date = i.meta_data.yayinlanma_tarihi || '';

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page,
			};
			return result;
		});
	};

	const setFeaturedItems = (data: IArticleCard[]) => {
		store.update((v) => ({ ...v, featuredItems: data, featuredItemsLoaded: true }));
	};

	return {
		subscribe: store.subscribe,
		setItems,
		setCategories,
		setActiveCategory,
		setPage,
		setFeaturedItems,
	};
}

const articleStore = handleArticleStore();

export { articleStore };
