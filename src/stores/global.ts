import { writable, get } from 'svelte/store';

function menuStore() {
  const menu = writable(false)
  
  const isActive = get(menu)

  const toggle = () => {
    menu.update(v => v = !v)
  }

  return {
    subscribe: menu.subscribe,
    toggle,
    isActive
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function headerStoreBuilder() {
  const data = {
    title: '',
    icon: '',
    backButton: false,
    share: false,
  }

  const page = writable(data)

  const pageInfo = get(page)

  const setPageTitle = (title: string) => {
    page.update(v => ({...v, title: title}))
  }
  const setPageIcon = (icon: string) => {
    page.update(v => ({...v, icon: icon}))
  }
  const setBack = (backButton: boolean) => {
    page.update(v => ({...v, backButton: backButton}))
  }
  const setPageShare = (share: boolean) => {
    page.update(v => ({...v, share: share}))
  }

  return {
    subscribe: page.subscribe,
    pageInfo,
    setPageTitle,
    setPageIcon,
    setBack,
  }
}

const menu = menuStore()
const headerStore = headerStoreBuilder()

export { menu, headerStore };
