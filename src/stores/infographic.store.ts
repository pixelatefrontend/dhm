import type { IInfographicMeta } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { ICategory, IInfographicCard } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IInfographicCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	activeCategoryId: number;
	categories: ICategory[];
};

function handleInfographicStore() {
	const data: TStore = {
		items: {},
		activeCategoryId: 0,
		categories: [
			{
				id: 0,
				slug: 'all',
				name: 'Hepsi'
			}
		]
	};

	const infographic = writable(data);

	const _infographic = get(infographic);

	const setCategories = async () => {
		if (_infographic.categories.length < 2) {
			const { items } = await getCategories('infographics');
			infographic.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1
				};
				const result = { ...v, categories: [...v.categories, ...items] };
				return result;
			});
		}
	};

	const setPage = (page: number, categoryId: number) => {
		infographic.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<IInfographicMeta>('infographics', {
			page,
			categoryId
		});
		infographic.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: IInfographicCard = {
					title: i.title,
					slug: i.slug
				};

				item.thumbnail = i.meta_data.image_details.medium.url || '';

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page
			};
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		infographic.update((v) => ({ ...v, activeCategoryId: id }));
	};

	return {
		subscribe: infographic.subscribe,
		setItems,
		setCategories,
		setActiveCategory,
		setPage
	};
}

const infographicStore = handleInfographicStore();

export { infographicStore };
