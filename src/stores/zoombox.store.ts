import { writable } from 'svelte/store';

type TStore = {
	zoom?: number;
	visible?: boolean;
	image: string;
};

function zoomboxStore() {
	const data: TStore = {
		zoom: 1,
		visible: false,
		image: ''
	};

	const zoombox = writable(data);

	const zoom = (val: number) => {
		zoombox.update((v) => ({ ...v, zoom: val }));
	};

	const toggle = (val: boolean) => {
		zoombox.update((v) => ({ ...v, visible: val }));
	};

	const setImage = (val: string) => {
		zoombox.update((v) => ({ ...v, image: val }));
	};

	return {
		subscribe: zoombox.subscribe,
		zoom,
		toggle,
		setImage
	};
}

const zoombox = zoomboxStore();

export { zoombox };
