import type { IHistoricalDocumentMeta } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { ICategory, IHistoricalDocumentCard } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IHistoricalDocumentCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	activeCategoryId: number;
	categories: ICategory[];
};

function handleHistoricalStore() {
	const data: TStore = {
		categories: [
			{
				id: 0,
				name: 'Hepsi'
			}
		],
		items: [],
		activeCategoryId: 0
	};

	const historical = writable(data);

	const _historical = get(historical);

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<IHistoricalDocumentMeta>('historicals', {
			page,
			categoryId
		});

		historical.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: IHistoricalDocumentCard = {
					title: i.title,
					slug: i.slug
				};

				item.subtitle = i.meta_data.belge_kaynagi || '';
				item.thumbnail = i.meta_data.image_details.medium.url || '';

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page
			};
			return result;
		});
	};

	const setCategories = async () => {
		if (_historical.categories.length < 2) {
			const { items } = await getCategories('historicals');
			historical.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1
				};
				const result = { ...v, categories: [...v.categories, ...items] };
				return result;
			});
		}
	};

	const setActiveCategory = (id: number) => {
		historical.update((v) => ({ ...v, activeCategoryId: id }));
	};

	const setPage = (page: number, categoryId: number) => {
		historical.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	return {
		subscribe: historical.subscribe,
		setItems,
		setCategories,
		setActiveCategory,
		setPage
	};
}

const historicalStore = handleHistoricalStore();

export { historicalStore };
