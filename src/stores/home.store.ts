import type { ISeo, THomeItem } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	items: THomeItem[];
	loaded: boolean;
	mobileItems: THomeItem[];
	mobilePage: number;
	mobileNext: boolean;
	seo: ISeo;
};

function handleHomeStore() {
	const data: TStore = {
		items: [],
		loaded: false,
		mobileItems: [],
		mobilePage: null,
		mobileNext: null,
		seo: null,
	};

	const store = writable(data);

	const setItems = (data: THomeItem[]) => {
		store.update((v) => ({ ...v, items: data }));
	};

	const setLoaded = (value: boolean) => {
		store.update((v) => ({ ...v, loaded: value }));
	};

	const setMobileItems = (data: THomeItem[]) => {
		store.update((v) => ({ ...v, mobileItems: data }));
	};

	const setMobileItemsMore = (data: THomeItem[]) => {
		store.update((v) => ({ ...v, mobileItems: [...v.mobileItems, ...data] }));
	};

	const setMobilePage = (value: number) => {
		store.update((v) => ({ ...v, mobilePage: value }));
	};

	const setMobileNext = (value: boolean) => {
		store.update((v) => ({ ...v, mobileNext: value }));
	};

	return {
		subscribe: store.subscribe,
		setItems,
		setLoaded,
		setMobileItems,
		setMobileItemsMore,
		setMobilePage,
		setMobileNext,
	};
}

const homeStore = handleHomeStore();

export { homeStore };
