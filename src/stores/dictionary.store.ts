import { removeTags } from '$lib/helpers';
import type { IDictionaryMeta, IDictionaryResponse } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { ICategory, IDictionaryCard } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IDictionaryCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	categories: ICategory[];
	activeCategoryId: number;
	letterSelected: boolean;
	lettersVisible: boolean;
	searchVisible: boolean;
	items: Item;
	highlights: IDictionaryCard[];
	highlightsLoaded: boolean;
	inDetail: boolean;
	searchActive: boolean;
};

function handleDictionaryStore() {
	const data: TStore = {
		categories: [
			{
				id: 0,
				name: 'Giriş'
			}
		],
		items: {},
		letterSelected: false,
		lettersVisible: true,
		searchVisible: true,
		searchActive: false,
		highlights: [],
		highlightsLoaded: false,
		activeCategoryId: 0,
		inDetail: false
	};

	const dictionary = writable(data);

	const _dictionary = get(dictionary);

	const setHighlights = (data: IDictionaryResponse[]) => {
		const items: IDictionaryCard[] = data.map((x) => {
			return {
				id: x.id,
				slug: x.slug,
				title: x.title.rendered,
				subtitle: x.meta_data.orjinal_ifade || '',
				description:
					x.content.rendered.length > 0 ? removeTags(x.content.rendered).slice(0, 250) + '...' : ''
			};
		});

		dictionary.update((v) => ({ ...v, highlights: [...items] }));
	};

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<IDictionaryMeta>('dictionary', {
			page,
			categoryId,
			categoryField: 'dictionary_letters',
			perPage: 100,
			extraParams: 'filter[orderby]=title&order=asc'
		});

		dictionary.update((v) => {
			const highlightedItems: IDictionaryCard[] = items
				.filter((i) => i.meta_data.one_cikarilmis_icerik === true)
				.map((x) => {
					return {
						title: x.title,
						slug: x.slug,
						subtitle: x.meta_data.orjinal_ifade || '',
						description: removeTags(x.content.rendered).slice(0, 250) + '...'
					};
				});

			const result = { ...v, highlights: [...highlightedItems] };

			const formattedItems = items.map((i) => {
				const item: IDictionaryCard = {
					title: i.title,
					slug: i.slug
				};

				item.subtitle = i.meta_data.orjinal_ifade || '';
				item.description = removeTags(i.content.rendered).slice(0, 250) + '...';

				return item;
			});

			items.forEach((i) => {
				if (i.dictionary_letters.length > 0) {
					i.dictionary_letters.forEach((l) => {
						v.items[l].items.push({
							title: i.title,
							slug: i.slug,
							subtitle: i.meta_data.orjinal_ifade || '',
							description: removeTags(i.content.rendered).slice(0, 250) + '...'
						});
					});
				}
			});

			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page
			};

			return result;
		});
	};

	const setCategories = async () => {
		if (_dictionary.categories.length < 2) {
			const { items } = await getCategories('dictionary');
			dictionary.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1
				};
				const result = { ...v, categories: [...v.categories, ...items] };
				return result;
			});
		}
	};

	const setPage = (page: number, categoryId: number) => {
		dictionary.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		dictionary.update((v) => ({ ...v, activeCategoryId: id, letterSelected: true }));
	};

	const setLettersVisible = (status: boolean) => {
		dictionary.update((v) => ({ ...v, lettersVisible: status }));
	};

	const setLetterSelected = (status: boolean) => {
		dictionary.update((v) => ({ ...v, letterSelected: status }));
	};

	const setSearchVisible = (status: boolean) => {
		dictionary.update((v) => ({ ...v, searchVisible: status }));
	};

	const setSearchActive = (status: boolean) => {
		dictionary.update((v) => ({ ...v, searchActive: status }));
	};

	const setInDetail = (status: boolean) => {
		dictionary.update((v) => ({ ...v, inDetail: status }));
	};

	const setHighlightsLoaded = (status: boolean) => {
		dictionary.update((v) => ({ ...v, highlightsLoaded: status }));
	};

	return {
		subscribe: dictionary.subscribe,
		setPage,
		setItems,
		setCategories,
		setSearchVisible,
		setLettersVisible,
		setLetterSelected,
		setActiveCategory,
		setInDetail,
		setSearchActive,
		setHighlights,
		setHighlightsLoaded
	};
}

const dictionaryStore = handleDictionaryStore();

export { dictionaryStore };
