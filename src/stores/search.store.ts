import type { ISearchResponse, TSection } from '$lib/types';
import { writable } from 'svelte/store';

interface Store {
	items: SearchResult[];
	visible: boolean;
	hasNext: boolean;
	loaded: boolean;
	loading: boolean;
	term: string;
	active?: boolean;
}

type SearchResult = {
	id: number;
	slug: string;
	title: string;
	section: TSection;
	thumbnail?: string;
	subtitle?: string;
	author?: string;
	date?: string;
	alive?: boolean;
	birth?: string;
	death?: string;
};

function handleSearchStore() {
	const data: Store = {
		items: [],
		hasNext: false,
		visible: false,
		loaded: false,
		loading: false,
		active: false,
		term: ''
	};

	const search = writable(data);

	const setItems = (items: ISearchResponse[]) => {
		const searches: SearchResult[] = items.map((i) => {
			const parts = i.url.split('/');
			const slug = parts.slice(-2).join('').toString();

			const result: SearchResult = {
				id: i.id,
				title: i.title,
				section: i.subtype,
				slug
			};

			if (result.section === 'bios') {
				result.subtitle =
					i['meta-data'].meslek_unvan || i['meta-data'].bio_categories?.[0]?.name || '';
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
				result.alive = i['meta-data']['yasiyor-mu'];
				result.birth =
					i['meta-data'].dogum_tarihi.split(' ').slice(-1).toString() ||
					i['meta-data'].dogum_tarihi_yil__donem ||
					'';
				result.death =
					i['meta-data'].olum_tarihi.split(' ').slice(-1).toString() ||
					i['meta-data'].olum_tarihi_yil__donem ||
					'';
			}

			if (result.section === 'exhibitions') {
				result.subtitle = i['meta-data'].exhibition_categories?.[0]?.name || '';
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'documentaries') {
				result.subtitle = i['meta-data'].yapimci || '';
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'maps') {
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'books') {
				result.subtitle = i['meta-data'].yazar || '';
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'infographics') {
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'virtual_tours') {
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'historical_documents') {
				result.subtitle =
					i['meta-data'].belge_kaynagi || i['meta-data'].document_categories?.[0]?.name || '';
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
			}

			if (result.section === 'articles') {
				result.thumbnail = i['meta-data'].image_details?.medium?.url || '';
				result.author = i['meta-data'].article_authors?.[0].name || '';
				result.date = i['meta-data'].yayinlanma_tarihi || '';
			}

			return result;
		});
		search.update((v) => ({ ...v, items: searches, loaded: true }));
	};

	const setNextPage = (status: boolean) => {
		search.update((v) => ({ ...v, hasNext: status }));
	};

	const setVisible = (status: boolean) => {
		search.update((v) => ({ ...v, visible: status }));
	};

	const setLoading = (status: boolean) => {
		search.update((v) => ({ ...v, loading: status }));
	};

	const setLoaded = (status: boolean) => {
		search.update((v) => ({ ...v, loaded: status }));
	};

	const setTerm = (text: string) => {
		search.update((v) => ({ ...v, term: text }));
	};

	const setSearchActive = (value: boolean) => {
		search.update((v) => ({ ...v, active: value }));
	};

	const clearStore = () => {
		search.set(data);
	};

	return {
		subscribe: search.subscribe,
		setItems,
		setNextPage,
		setVisible,
		clearStore,
		setLoading,
		setLoaded,
		setTerm,
		setSearchActive
	};
}

const searchStore = handleSearchStore();

const searchTerm = writable('');

export { searchStore, searchTerm };
