import type { ILibraryMeta } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { ICategory, ILibraryCard } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: ILibraryCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	categories: ICategory[];
	activeCategoryId: number;
	items: Item;
};

function handleLibraryStore() {
	const data: TStore = {
		categories: [
			{
				id: 0,
				name: 'Hepsi'
			}
		],
		items: {},
		activeCategoryId: 0
	};

	const library = writable(data);

	const _library = get(library);

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<ILibraryMeta>('library', {
			page,
			categoryId
		});
		library.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: ILibraryCard = {
					title: i.title,
					slug: i.slug
				};

				item.thumbnail = i.meta_data.image_details.medium.url || '';
				item.subtitle = i.meta_data.yazar || '';

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page
			};
			return result;
		});
	};

	const setCategories = async () => {
		if (_library.categories.length < 2) {
			const { items } = await getCategories('library');
			library.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1
				};
				const result = { ...v, categories: [ ...v.categories, ...items] };
				return result;
			});
		}
	};

	const setPage = (page: number, categoryId: number) => {
		library.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		library.update((v) => ({ ...v, activeCategoryId: id }));
	};

	return {
		subscribe: library.subscribe,
		setItems,
		setCategories,
		setActiveCategory,
		setPage
	};
}

const libraryStore = handleLibraryStore();

export { libraryStore };
