import type { IDocumentaryMeta } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { ICategory, IDocumentaryCard } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IDocumentaryCard[];
	page: number;
	hasNext: boolean;
	isLoaded: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	categories: ICategory[];
	activeCategoryId: number;
};

function handleDocumentaryStore() {
	const data: TStore = {
		items: {},
		activeCategoryId: 0,
		categories: [
			{
				id: 0,
				name: 'Hepsi'
			}
		]
	};

	const documentaries = writable(data);

	const _documentaries = get(documentaries);

	const setCategories = async () => {
		if (_documentaries.categories.length < 2) {
			const { items } = await getCategories('documentaries');
			documentaries.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						isLoaded: false,
						page: 1
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					isLoaded: false,
					page: 1
				};
				const result = { ...v, categories: [...v.categories, ...items] };
				return result;
			});
		}
	};

	const setPage = (page: number, categoryId: number) => {
		documentaries.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setItems = async (categoryId: number, page: number) => {
		const { items, hasNext } = await getList<IDocumentaryMeta>('documentaries', {
			page,
			categoryId
		});
		documentaries.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: IDocumentaryCard = {
					title: i.title,
					slug: i.slug
				};

				item.thumbnail = i.meta_data.image_details.medium.url || '';
				item.subtitle = i.meta_data.yapimci || '';

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				isLoaded: true,
				page
			};
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		documentaries.update((v) => ({ ...v, activeCategoryId: id }));
	};

	return {
		subscribe: documentaries.subscribe,
		setCategories,
		setItems,
		setActiveCategory,
		setPage
	};
}

const documentaryStore = handleDocumentaryStore();

export { documentaryStore };
