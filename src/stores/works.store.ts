import type { ExhibitionResponse } from '$lib/response.type';
import type { Work } from '$lib/types';
import { urlify } from '$lib/utils';
import { get, writable } from 'svelte/store';

type WorksStore = {
	list: Work[];
	page: number;
	hasNext: boolean;
	slug: string;
	loaded: boolean;
};

function handleWorksStore() {
	const data: WorksStore = {
		list: [],
		page: 1,
		slug: '',
		hasNext: true,
		loaded: false
	};

	const works = writable(data);

	const worksData = get(works);

	const setList = (data: ExhibitionResponse) =>
		works.update((v) => {
			let modified: Work[] = [];
			if (data.meta_data.secili_eserler) {
				modified = data.meta_data.secili_eserler.map((d) => {
					const result: Work = {
						id: d.ID,
						name: d.post_title
					};

					if (d.featured_media.thumbnail.url) {
						result.thumbnail = d.featured_media.thumbnail.url;
					}

					if (d.featured_media.full.url) {
						result.fullImage = d.featured_media.full.url;
					}

					return result;
				});
			}
			return {
				...v,
				list: [...v.list, ...modified],
				loaded: true
			};
		});

	const setNextPage = (page: number, status: boolean) =>
		works.update((v) => ({ ...v, page, hasNext: status }));

	const loadMore = async () => {
		const page = worksData.page + 1;
		const request = await fetch(
			urlify({
				section: 'exhibitions',
				fields: 'title,meta_data',
				perPage: 10,
				page,
				slug: worksData.slug
			})
		);

		const hasNext = parseInt(request.headers.get('x-wp-totalpages')) !== page;
		const response: ExhibitionResponse[] = await request.json();

		setList(response[0]);
		setNextPage(page, hasNext);
	};

	return {
		subscribe: works.subscribe,
		setList,
		setNextPage,
		loadMore
	};
}

const worksStore = handleWorksStore();

export { worksStore };
