import { get, writable } from 'svelte/store';

function headerbarStoreBuilder() {
	const data = {
		title: '',
		icon: 'parmak_izi',
		back: false,
		share: false,
		search: false,
		visible: true,
		searchVisible: false
	};

	const header = writable(data);

	const headerbar = get(header);

	const setTitle = (title: string) => {
		header.update((v) => ({ ...v, title: title }));
	};

	const setIcon = (icon: string) => {
		header.update((v) => ({ ...v, icon: icon }));
	};

	const setBack = (status: boolean) => {
		header.update((v) => ({ ...v, back: status }));
	};

	const setSearch = (status: boolean) => {
		header.update((v) => ({ ...v, search: status }));
	};

	const setShare = (status: boolean) => {
		header.update((v) => ({ ...v, share: status }));
	};

	const setVisible = (status: boolean) => {
		header.update((v) => ({ ...v, visible: status }));
	};

	const setSearchVisible = (status: boolean) => {
		header.update((v) => ({ ...v, searchVisible: status }));
	};

	return {
		subscribe: header.subscribe,
		headerbar,
		setTitle,
		setIcon,
		setBack,
		setSearch,
		setShare,
		setVisible,
		setSearchVisible,
	};
}

const headerStore = headerbarStoreBuilder();

export { headerStore };

