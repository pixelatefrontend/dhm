import { writable } from 'svelte/store';

function sidebarStoreBuilder() {
	const data = {
		menuVisible: false,
		offsetTop: 0,
		statsHeight: 0,
		focusText: 'her şey',
		homeOffset: 0,
		subpageOffset: 0,
		infoVisible: false,
	};

	const sidebar = writable(data);

	const setOffset = (value: number) => {
		sidebar.update((v) => ({ ...v, offsetTop: value }));
	};

	const setStatsHeight = (value: number) => {
		sidebar.update((v) => ({ ...v, statsHeight: value }));
	};

	const setMenuVisible = (value: boolean) => {
		sidebar.update((v) => ({ ...v, menuVisible: value }));
	};

	const setFocusText = (value: string) => {
		sidebar.update((v) => ({ ...v, focusText: value }));
	};

	const setHomeOffset = (value: number) => {
		sidebar.update((v) => ({ ...v, homeOffset: value }));
	};

	const setSubpageOffset = (value: number) => {
		sidebar.update((v) => ({ ...v, subpageOffset: value }));
	};

	const setInfoVisible = (value: boolean) => {
		sidebar.update((v) => ({ ...v, infoVisible: value }));
	};

	return {
		subscribe: sidebar.subscribe,
		setOffset,
		setMenuVisible,
		setFocusText,
		setStatsHeight,
		setHomeOffset,
		setSubpageOffset,
		setInfoVisible
	};
}

const sidebarStore = sidebarStoreBuilder();

export { sidebarStore };
