import { APP_PAGES } from '$lib/constants/pages';
import { API_STORE } from '$lib/constants/urls';
import type { ISocialResponse, IStatsResponse } from '$lib/response.type';
import { getPages, getPageSpots } from '$lib/services';
import type { ISeo, SectionType, TLink, TPlatform, TStat } from '$lib/types';
import { urlify } from '$lib/utils';
import { get, writable } from 'svelte/store';

type Store = {
	title: string;
	text: string;
	backUrl: string;
	dimensions: {
		width: number;
		height: number;
	};
	inDetail: boolean;
	headerHeight: number;
	visibleShareBox: boolean;
	visibleCopyText: boolean;
	currentSection: SectionType;
	isLoading: boolean;
	isNavigated: boolean;
	scrollY: number;
	spots: {
		[key in SectionType]?: {
			title?: string;
			description?: string;
			seoTitle?: string;
			icon?: string;
			seo?: ISeo;
		};
	};
	socials: {
		[key in TPlatform]?: TLink;
	};
	stats: TStat[];
	staticPages?: string[];
	isWindowLoaded: boolean;
	introEnabled: boolean;
	isRemoveIntro: boolean;
	introVideoLoaded: boolean;
	introTextsVisible: boolean;
	fonstsLoaded: boolean;
};

type Dimensions = {
	width: number;
	height: number;
};

function pageStoreBuilder() {
	const data: Store = {
		title: 'Filistin ile ilgili bütün gerçekler',
		text: 'Arşivlerde kayıtlı olan 1.000’den fazla veriyi ve daha fazlasını keşfet.',
		backUrl: '/',
		dimensions: {
			width: 0,
			height: 0,
		},
		scrollY: 0,
		headerHeight: 0,
		visibleShareBox: false,
		visibleCopyText: false,
		currentSection: 'home',
		isLoading: true,
		isNavigated: false,
		inDetail: false,
		spots: {},
		socials: {},
		stats: [],
		staticPages: ['/hakkimizda', '/projeler', '/iletisim'],
		isWindowLoaded: false,
		isRemoveIntro: false,
		introEnabled: true,
		introVideoLoaded: false,
		introTextsVisible: true,
		fonstsLoaded: false,
	};

	const page = writable(data);

	const pageData = get(page);

	const setTitle = (title: string) => {
		page.update((v) => ({ ...v, title }));
	};

	const setNavigated = (status: boolean) => {
		page.update((v) => ({ ...v, isNavigated: status }));
	};

	const setInDetail = (status: boolean) => {
		page.update((v) => ({ ...v, inDetail: status }));
	};

	const setText = (text: string) => {
		page.update((v) => ({ ...v, text }));
	};

	const setBackUrl = (text: string) => {
		page.update((v) => ({ ...v, backUrl: text }));
	};

	const setLoading = (status: boolean) => {
		page.update((v) => ({ ...v, isLoading: status }));
	};

	const setDimensions = (value: Dimensions) => {
		page.update((v) => ({ ...v, dimensions: value }));
	};

	const setHeaderHeight = (value: number) => {
		page.update((v) => ({ ...v, headerHeight: value }));
	};

	const setInitialTexts = () => {
		page.update((v) => {
			return {
				...v,
				text: data.text,
				title: data.title,
			};
		});
	};
	const setVisibleShareBox = (value: boolean) => {
		page.update((v) => ({ ...v, visibleShareBox: value }));
	};
	const setVisibleCopyText = (value: boolean) => {
		page.update((v) => ({ ...v, visibleCopyText: value }));
	};

	const setCurrentSection = (section: SectionType) => {
		page.update((v) => ({ ...v, currentSection: section }));
	};

	const setSpotTexts = async (section: SectionType) => {
		if (!section) return console.error('No section provided');

		try {
			const { content } = await getPageSpots(section);
			page.update((v) => {
				const result = { ...v };
				result.spots[section] = {
					title: content.meta_data.spot_baslik,
					description: content.meta_data.aciklama,
					seoTitle: content.yoast_head_json.title,
					icon: content.meta_data.ikon?.url || '',
				};
				return result;
			});
		} catch (error) {
			throw new Error(`Error while setting spot texts: \n${error}`);
		}
	};

	const setSpots = async () => {
		const { spots } = get(pageStore);
		if (Object.keys(spots).length > 1) {
			return;
		}

		try {
			const { items } = await getPages();
			page.update((v) => {
				const result = { ...v };
				items.forEach((e) => {
					const key = APP_PAGES.find((p) => p.slug === e.slug).key;
					const item = items.find((i) => i.slug === e.slug);
					result.spots[key] = {
						icon: item.meta_data.ikon?.url || '',
						title: item.meta_data.spot_baslik || '',
						description: item.meta_data.aciklama || '',
						seoTitle: item.yoast_head_json?.title || '',
					};
				});
				return result;
			});
		} catch (error) {
			throw new Error(`Error while setting pages data: \n${error}`);
		}
	};

	const setSocials = async () => {
		if (Object.keys(pageData.socials).length > 0) {
			return;
		}

		try {
			const request = await fetch(API_STORE.socials);
			const response: ISocialResponse = await request.json();
			page.update((v) => {
				const result = { ...v };
				const socialKeys = Object.keys(response.sosyal_medya);
				result.spots = { ...result.spots, home: {} };
				result.spots['home'].title = response.ana_sayfa_spot;
				result.spots['home'].description = response.ana_sayfa_aciklama;
				result.spots['home'].seoTitle = 'Dijital Hafıza Merkezi - ' + response.ana_sayfa_aciklama;
				result.spots['home'].icon = '';

				socialKeys.forEach((s) => {
					result.socials[s] = response.sosyal_medya[s];
				});
				return result;
			});
		} catch (error) {
			throw new Error(`Error while getting social data: \n ${error}`);
		}
	};

	const setStats = async () => {
		try {
			const URL = urlify({ section: 'stats', fields: 'slug,meta_data' });
			const request = await fetch(URL);
			const response: IStatsResponse[] = await request.json();
			page.update((v) => {
				const result = { ...v };
				const items: TStat[] = response.map((s) => ({
					name: s.slug,
					description: s.meta_data.aciklama,
					value: s.meta_data.istatistik,
					icon: s.meta_data.ikon.url,
				}));
				result.stats = [...items];
				return result;
			});
		} catch (error) {
			throw new Error(`Error while getting stats: \n${error}`);
		}
	};

	const setWindowLoaded = (status: boolean) => {
		page.update((v) => ({ ...v, isWindowLoaded: status }));
	};

	const setRemoveIntro = (status: boolean) => {
		page.update((v) => ({ ...v, isRemoveIntro: status }));
	};

	const setIntroEnabled = (status: boolean) => {
		page.update((v) => ({ ...v, introEnabled: status }));
	};

	const setIntroVideoLoaded = (status: boolean) => {
		page.update((v) => ({ ...v, introVideoLoaded: status }));
	};

	const setIntroTextsVisible = (status: boolean) => {
		page.update((v) => ({ ...v, introTextsVisible: status }));
	};

	const setScrollY = (value: number) => {
		page.update((v) => ({ ...v, scrollY: value }));
	};

	const setFontsLoaded = (value: boolean) => {
		page.update((v) => ({ ...v, fonstsLoaded: value }));
	};

	const setSeoContent = (section: SectionType, data: ISeo) => {
		page.update((v) => {
			const result = { ...v };
			result.spots[section].seo = data;
			return result;
		});
	};

	return {
		subscribe: page.subscribe,
		pageData,
		setInDetail,
		setNavigated,
		setSpotTexts,
		setTitle,
		setText,
		setBackUrl,
		setLoading,
		setDimensions,
		setHeaderHeight,
		setInitialTexts,
		setVisibleShareBox,
		setCurrentSection,
		setVisibleCopyText,
		setSocials,
		setStats,
		setSpots,
		setWindowLoaded,
		setRemoveIntro,
		setIntroEnabled,
		setIntroVideoLoaded,
		setIntroTextsVisible,
		setScrollY,
		setSeoContent,
		setFontsLoaded,
	};
}

const pageStore = pageStoreBuilder();

export { pageStore };

