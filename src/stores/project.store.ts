import type { IProject } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	items: IProject[];
	total: number;
	page: number;
	hasNext: boolean;
};

function handleProjectsStore() {
	const data: TStore = {
		items: [],
		total: null,
		page: null,
		hasNext: null,
	};

	const store = writable(data);

	const setItems = (data: IProject[]) => {
		store.update((v) => ({ ...v, items: data }));
	};

	const setMoreItems = (data: IProject[]) => {
		store.update((v) => ({ ...v, items: [...v.items, ...data] }));
	};

	const setTotal = (value: number) => {
		store.update((v) => ({ ...v, total: value }));
	};

	const setNext = (value: boolean) => {
		store.update((v) => ({ ...v, hasNext: value }));
	};

	const setPage = (value: number) => {
		store.update((v) => ({ ...v, page: value }));
	};

	return {
		subscribe: store.subscribe,
		setPage,
		setNext,
		setTotal,
		setItems,
		setMoreItems
	};
}

const projectsStore = handleProjectsStore();

export { projectsStore };
