import type { ITimelineResponse } from '$lib/response.type';
import { getContentAll } from '$lib/services';
import type { ITimelineContent, ITimelineDate, ITimelineItem } from '$lib/types';
import { writable } from 'svelte/store';
import type Swiper from 'swiper';

type TimelineStoreProps = {
	activeIndex: number;
	mounted: boolean;
	activeDate: string;
	dates: string[];
	events: ITimelineContent[];
	eventsSlider: Swiper;
	thumbsSlider: Swiper;
	total: number;
	page: number;
	hasNext: boolean;
	loading: boolean;
	dateIndexes: string[];
	inDetail: boolean;
	timeline: ITimelineDate[];
	fromHome: boolean;
	items: ITimelineItem[];
	currentEvent: ITimelineContent;
};

function timelineStoreHandler() {
	const data: TimelineStoreProps = {
		activeIndex: null,
		total: null,
		page: 1,
		mounted: false,
		activeDate: '',
		eventsSlider: null,
		thumbsSlider: null,
		dates: [],
		events: [],
		items: [],
		hasNext: false,
		loading: false,
		inDetail: false,
		dateIndexes: [],
		timeline: [],
		fromHome: false,
		currentEvent: null,
	};

	const timeline = writable(data);

	const setActiveIndex = (index: number) => timeline.update((v) => ({ ...v, activeIndex: index }));

	const setMounted = (status: boolean) => timeline.update((v) => ({ ...v, mounted: status }));

	const setActiveDate = (value: string) => timeline.update((v) => ({ ...v, activeDate: value }));

	const setDates = (value: string[]) => timeline.update((v) => ({ ...v, dates: value }));

	const setEventsSlider = (el: Swiper) => timeline.update((v) => ({ ...v, eventsSlider: el }));

	const setThumbsSlider = (el: Swiper) => timeline.update((v) => ({ ...v, thumbsSlider: el }));

	const setSliderActiveIndex = (index: number) => {
		timeline.update((v) => ({ ...v, activeIndex: index }));
	};

	const setEvents = async () => {
		const { content, total } = await getContentAll<ITimelineResponse>('timeline');
		const formattedItems: ITimelineContent[] = content.map((i) => {
			return {
				id: i.id,
				title: i.title.rendered,
				slug: i.slug,
				year: i.meta_data.olay_tarihi?.split(' ').slice(-1).toString(),
				date: i.meta_data.olay_tarihi || '',
				content: i.content.rendered,
				thumbnail: i.meta_data.image_details.medium?.url || '',
				fullImage: i.meta_data.image_details.full?.url || '',
				videoImage: i.meta_data.video_icerik.kapak_gorseli.url || '',
				videoId: i.meta_data.video_icerik.video_id || '',
				dateUnknown: i.meta_data.belirsiz,
				seo: {
					title: i.yoast_head_json.title,
				},
			};
		});

		const formattedYears = formattedItems.map((e) => e.year);

		timeline.update((v) => ({ ...v, events: formattedItems, dates: formattedYears, total }));
	};

	const setItems = (data: ITimelineItem[]) => {
		timeline.update((v) => ({ ...v, items: data }));
	};

	const setItemsMore = (data: ITimelineItem[]) => {
		timeline.update((v) => ({ ...v, items: [...v.items, ...data] }));
	};

	const setMoreItems = (data: ITimelineContent[]) => {
		timeline.update((v) => ({ ...v, events: [...v.events, ...data] }));
	};

	const setYears = (data: ITimelineContent[]) => {
		const formattedYears = data.map((e) => e.year);
		timeline.update((v) => ({ ...v, dates: formattedYears }));
	};

	const setMoreYears = (data: ITimelineContent[]) => {
		const formattedYears = data.map((e) => e.year);
		timeline.update((v) => ({ ...v, dates: [...v.dates, ...formattedYears] }));
	};

	const setPage = (value: number) => {
		timeline.update((v) => ({ ...v, page: value }));
	};

	const setNext = (value: boolean) => {
		timeline.update((v) => ({ ...v, hasNext: value }));
	};

	const setLoading = (value: boolean) => {
		timeline.update((v) => ({ ...v, loading: value }));
	};

	const setTotal = (value: number) => {
		timeline.update((v) => ({ ...v, total: value }));
	};

	const setInDetail = (value: boolean) => {
		timeline.update((v) => ({ ...v, inDetail: value }));
	};

	const setDateIndexes = () => {
		timeline.update((v) => {
			return {
				...v,
				dateIndexes: v.timeline.map((e) =>
					v.timeline.findIndex((t) => t.year === e.year).toString()
				),
			};
		});
	};

	const setTimelineDates = (data: ITimelineDate[]) => {
		timeline.update((v) => ({ ...v, timeline: data }));
	};

	const setFromHome = (value: boolean) => {
		timeline.update((v) => ({ ...v, fromHome: value }));
	};

	const setCurrentEvent = (value: ITimelineContent) => {
		timeline.update((v) => ({ ...v, currentEvent: value }));
	};

	return {
		subscribe: timeline.subscribe,
		setActiveIndex,
		setMounted,
		setActiveDate,
		setEventsSlider,
		setThumbsSlider,
		setSliderActiveIndex,
		setDates,
		setEvents,
		setItems,
		setItemsMore,
		setYears,
		setMoreItems,
		setPage,
		setNext,
		setMoreYears,
		setLoading,
		setTotal,
		setDateIndexes,
		setInDetail,
		setTimelineDates,
		setFromHome,
		setCurrentEvent
	};
}

const timelineStore = timelineStoreHandler();

export { timelineStore };
