import type { IBioMeta } from '$lib/response.type';
import { getCategories, getList } from '$lib/services';
import type { IBiographyCard, ICategory } from '$lib/types';
import { get, writable } from 'svelte/store';

type List = {
	items: IBiographyCard[];
	page: number;
	hasNext: boolean;
	isLoading?: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	categories: ICategory[];
	activeCategoryId: number;
};

function handleBioStore() {
	const data: TStore = {
		items: {},
		activeCategoryId: 0,
		categories: [
			{
				id: 0,
				name: 'Hepsi'
			}
		]
	};

	const bios = writable(data);

	const _bios = get(bios);

	const setCategories = async () => {
		if (_bios.categories.length < 2) {
			const { items } = await getCategories('bios');
			bios.update((v) => {
				items.forEach((i) => {
					v.items[i.id] = {
						items: [],
						hasNext: true,
						page: 1,
						isLoading: null
					};
				});
				v.items[0] = {
					items: [],
					hasNext: true,
					page: 1,
					isLoading: null
				};
				const result = { ...v, categories: [...items] };
				return result;
			});
		}
	};

	const setItems = async (categoryId: number, page: number) => {
		setLoading(categoryId, true);
		const { items, hasNext } = await getList<IBioMeta>('bios', {
			page,
			categoryId,
			extraParams: 'orderby=rand&skip-cache=1'
		});
		bios.update((v) => {
			const result = { ...v };
			const formattedItems = items.map((i) => {
				const item: IBiographyCard = {
					title: i.title,
					slug: i.slug
				};

				item.birth =
					i.meta_data.dogum_tarihi.split(' ').slice(-1).toString() ||
					i.meta_data.dogum_tarihi_yil__donem ||
					'';
				item.death = i.meta_data.olum_tarihi.split(' ').slice(-1).toString() || i.meta_data.olum_tarihi_yil__donem || '';
				item.thumbnail = i.meta_data.image_details.medium.url || '';
				item.subtitle = i.meta_data.meslek_unvan || i.meta_data.bio_categories?.[0]?.name || '';
				item.alive = i.meta_data['yasiyor-mu']

				return item;
			});
			result.items[categoryId] = {
				items: [...v.items[categoryId].items, ...formattedItems],
				hasNext,
				page
			};
			return result;
		});
		setLoading(categoryId, false);
	};

	const setPage = (page: number, categoryId: number) => {
		bios.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page + 1;
			result.items = v.items;
			return result;
		});
	};

	const setLoading = (categoryId: number, status: boolean) => {
		bios.update((v) => {
			const result = { ...v };
			v.items[categoryId].isLoading = status;
			result.items = v.items;
			return result;
		});
	};

	return {
		subscribe: bios.subscribe,
		setCategories,
		setItems,
		setPage,
		setLoading
	};
}

const bioStore = handleBioStore();

export { bioStore };
