import type { ICategory, IExhibitionCard } from '$lib/types';
import { writable } from 'svelte/store';

type List = {
	items?: IExhibitionCard[];
	page?: number;
	hasNext?: boolean;
	isLoaded?: boolean;
};

type Item = {
	[key: number]: List;
};

type TStore = {
	items: Item;
	categories: ICategory[];
	activeCategoryId: number;
	detailHeader: string;
};

function handleExhibitionStore() {
	const data: TStore = {
		items: {},
		activeCategoryId: 0,
		categories: [],
		detailHeader: null,
	};

	const store = writable(data);

	const setCategories = (data: ICategory[]) => {
		store.update((v) => {
			const result = { ...v };
			result.categories = data;
			result.categories.push({
				id: -1,
				name: 'Sanatçılar',
			});
			result.activeCategoryId = data[0].id;
			return result;
		});
	};

	const setItemCategories = (data: ICategory[]) => {
		store.update((v) => {
			const result = { ...v };
			data.forEach((e) => {
				result.items[e.id] = {
					hasNext: true,
					items: [],
					page: 1,
				};
			});
			return result;
		});
	};

	const setItems = (data: IExhibitionCard[], categoryId: number) => {
		store.update((v) => {
			const result = { ...v };
			result.items[categoryId].items = data;
			return result;
		});
	};

	const setMoreItems = (data: IExhibitionCard[], categoryId: number) => {
		store.update((v) => {
			const result = { ...v };
			result.items[categoryId].items = [...v.items[categoryId].items, ...data];
			return result;
		});
	};

	const setPage = (page: number, categoryId: number) => {
		store.update((v) => {
			const result = { ...v };
			v.items[categoryId].page = page;
			result.items = v.items;
			return result;
		});
	};

	const setNext = (value: boolean, categoryId: number) => {
		store.update((v) => {
			const result = { ...v };
			v.items[categoryId].hasNext = value;
			result.items = v.items;
			return result;
		});
	};

	const setActiveCategory = (id: number) => {
		store.update((v) => ({ ...v, activeCategoryId: id }));
	};

	const setDetailHeader = (value: string) => {
		store.update((v) => ({ ...v, detailHeader: value }));
	};

	return {
		subscribe: store.subscribe,
		setCategories,
		setItemCategories,
		setItems,
		setMoreItems,
		setPage,
		setNext,
		setActiveCategory,
		setDetailHeader,
	};
}

const exhibitionStore = handleExhibitionStore();

export { exhibitionStore };
