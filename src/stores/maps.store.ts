import type { IMapResponse } from '$lib/response.type';
import type { IMap, IMapCategory, MapTypes, TMap } from '$lib/types';
import { writable } from 'svelte/store';

type CategorizedMaps = {
	page: number;
	hasNext: boolean;
	total: number;
	list: IMap[];
};

type TStore = {
	categories: IMapCategory[];
	activeCategoryId: number;
	maps: {
		[key in MapTypes]: CategorizedMaps;
	};
};

function handleMapsStore() {
	const data: TStore = {
		maps: {
			schematic: {
				page: 1,
				hasNext: true,
				total: null,
				list: [],
			},
			interactive: {
				page: 1,
				hasNext: true,
				total: null,
				list: [],
			},
		},
		categories: [],
		activeCategoryId: null,
	};

	const store = writable(data);

	const setMaps = async (type: MapTypes, data: IMapResponse[], next: boolean, page) => {
		const items: IMap[] = data.map((i) => {
			return {
				id: i.id,
				title: i.title.rendered,
				slug: i.slug,
				thumbnail: i.meta_data.image_details.thumbnail.url || '',
			};
		});

		store.update((v) => {
			const result = { ...v };

			result.maps[type].hasNext = next;
			result.maps[type].page = page;
			result.maps[type].list = [...result.maps[type].list, ...items];

			return result;
		});
	};

	const setNextPage = (type: MapTypes) => {
		store.update((v) => {
			const result = { ...v };
			v[type].page = v[type].page + 1;
			result[type] = v[type];
			return result;
		});
	};

	const setItems = (category: TMap, data: IMap[]) => {
		store.update((v) => {
			const result = { ...v };
			result.maps[category].list = data;
			return result;
		});
	};

	const setMoreItems = (category: TMap, data: IMap[]) => {
		store.update((v) => {
			const result = { ...v };
			result.maps[category].list = [...result.maps[category].list, ...data];
			return result;
		});
	};

	const setTotal = (category: TMap, value: number) => {
		store.update((v) => {
			const result = { ...v };
			result.maps[category].total = value;
			return result;
		});
	};

	const setNext = (category: TMap, value: boolean) => {
		store.update((v) => {
			const result = { ...v };
			result.maps[category].hasNext = value;
			return result;
		});
	};

	const setPage = (category: TMap, value: number) => {
		store.update((v) => {
			const result = { ...v };
			result.maps[category].page = value;
			return result;
		});
	};

	const setMapCategories = (data: IMapCategory[]) => {
		store.update((v) => ({ ...v, categories: data }));
	};

	const setActiveCategory = (value: number) => {
		store.update((v) => ({ ...v, activeCategoryId: value }));
	};

	return {
		subscribe: store.subscribe,
		setMaps,
		setMapCategories,
		setNextPage,
		setTotal,
		setNext,
		setPage,
		setItems,
		setMoreItems,
		setActiveCategory,
	};
}

const mapsStore = handleMapsStore();

export { mapsStore };
