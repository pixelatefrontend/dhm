import { writable } from 'svelte/store';

type NavigationState = 'loading' | 'loaded' | null;

const navigationStore = writable<NavigationState>(null);

export { navigationStore };
