import { writable } from 'svelte/store';

type TStore = {
	twitter?: string;
	facebook?: string;
	linkedin?: string;
};

function handleShareStore() {
	const data: TStore = {
		twitter: '',
		facebook: '',
		linkedin: ''
	};

	const share = writable(data);

	const setFacebook = (url: string) => {
		share.update((v) => ({
			...v,
			facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + url
		}));
	};

	const setTwitter = (url: string, text?: string) => {
		share.update((v) => ({
			...v,
			twitter: 'https://twitter.com/intent/tweet?url=' + url + '&text=' + encodeURI(text)
		}));
	};

	const setLinkedin = (url: string) => {
		share.update((v) => ({
			...v,
			linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url=' + url
		}));
	};

	return {
		subscribe: share.subscribe,
		setFacebook,
		setTwitter,
		setLinkedin
	};
}

const shareStore = handleShareStore();

export { shareStore };
