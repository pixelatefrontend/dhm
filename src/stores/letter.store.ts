import type { IDictionaryCard } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	id: number;
	items: IDictionaryCard[];
	total: number;
	page: number;
	hasNext: boolean;
};

function handleLetterStore() {
	const data: TStore = {
		id: null,
		items: [],
		total: null,
		page: 1,
		hasNext: false
	};

	const store = writable(data);

	const setId = (value: number) => {
		store.update((v) => ({ ...v, id: value }));
	};

	const setItems = (data: IDictionaryCard[]) => {
		store.update((v) => ({ ...v, items: data }));
	};

	const setMoreItems = (data: IDictionaryCard[]) => {
		store.update((v) => ({ ...v, items: [...v.items, ...data] }));
	};

	const setTotal = (value: number) => {
		store.update((v) => ({ ...v, total: value }));
	};

	const setNext = (value: boolean) => {
		store.update((v) => ({ ...v, hasNext: value }));
	};

	const setPage = (value: number) => {
		store.update((v) => ({ ...v, page: value }));
	};

	return {
		subscribe: store.subscribe,
		setId,
		setPage,
		setNext,
		setTotal,
		setItems,
    setMoreItems,
	};
}

const letterStore = handleLetterStore();

export { letterStore };
