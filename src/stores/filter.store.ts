import type { SectionType, TOrderType } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	currentFilter?: TOrderType;
	filteredItems: {
		[key in SectionType]?: {
			[key in TOrderType]?: {
				list: any[];
				loading: boolean;
				hasNext: boolean;
				page: number;
			};
		};
	};
};

function handleFilterStore() {
	const data: TStore = {
		currentFilter: undefined,
		filteredItems: {
			bios: {
				nameAsc: {
					list: [],
					loading: false,
					hasNext: false,
					page: 1
				},
				nameDesc: {
					list: [],
					loading: false,
					hasNext: false,
					page: 1
				},
				birthAsc: {
					list: [],
					loading: false,
					hasNext: false,
					page: 1
				},
				birthDesc: {
					list: [],
					loading: false,
					hasNext: false,
					page: 1
				}
			}
		}
	};

	const filters = writable(data);

	const setCurrentFilter = (val: TOrderType) =>
		filters.update((v) => ({ ...v, currentFilter: val }));

	return {
		subscribe: filters.subscribe,
    setCurrentFilter,
	};
}

const filterStore = handleFilterStore();

export { filterStore };
