import { PAGE_URLS } from '$lib/constants/urls';
import type { ISeo, SectionType } from '$lib/types';
import { writable } from 'svelte/store';

type TStore = {
	[key in SectionType]?: ISeo;
};

function handleSeoStore() {
	const data: TStore = {};

	const store = writable(data);

	const buildStore = () => {
		const items: TStore = {};
		Object.keys(PAGE_URLS).forEach((e) => {
			items[e] = {};
		});
		store.set(items);
	};

	const setSeoContent = (section: SectionType, data: ISeo) => {
		store.update((v) => {
			v[section] = data;
			return v;
		});
	};

	return {
		subscribe: store.subscribe,
		buildStore,
		setSeoContent,
	};
}

const seoStore = handleSeoStore();

export { seoStore };
