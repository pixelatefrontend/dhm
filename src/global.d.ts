/// <reference types="@sveltejs/kit" />

interface ImportMetaEnv {
	readonly VITE_APP_LOCALE: string;
	readonly VITE_APP_PLATFORM: string;
	readonly VITE_MAIL_HOST: string;
	readonly VITE_MAIL_PORT: string;
	readonly VITE_MAIL_USERNAME: string;
	readonly VITE_MAIL_PASSWORD: string;
}

interface ImportMeta {
	readonly env: ImportMetaEnv;
}
