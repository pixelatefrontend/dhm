import type { Date } from "$lib/types";

const EVENTS: Date[] = [
	{
		date: '23 Nisan 1920'
	},
	{
		date: '18 Aralık 1920'
	},
	{
		date: '10 Ocak 1998'
	},
	{
		date: '10 Haziran 1998'
	},
	{
		date: '10 Aralık 1998'
	}
];

export { EVENTS };
