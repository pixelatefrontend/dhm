const LETTERS: DictionaryLetter[] = [
	{
		key: '',
		value: 'HEPSİ'
	},
	{
		key: 'a',
		value: 'a'
	},
	{
		key: 'b',
		value: 'b'
	},
	{
		key: 'c',
		value: 'c'
	},
	{
		key: 'd',
		value: 'd'
	},
	{
		key: 'e',
		value: 'e'
	},
	{
		key: 'f',
		value: 'f'
	},
	{
		key: 'g',
		value: 'g'
	},
	{
		key: 'h',
		value: 'h'
	},
	{
		key: 'ı',
		value: 'ı'
	},
	{
		key: 'j',
		value: 'j'
	},
	{
		key: 'k',
		value: 'k'
	},
	{
		key: 'l',
		value: 'l'
	},
	{
		key: 'm',
		value: 'm'
	},
	{
		key: 'n',
		value: 'n'
	},
	{
		key: 'o',
		value: 'o'
	},
	{
		key: 'p',
		value: 'p'
	},
	{
		key: 'q',
		value: 'q'
	},
	{
		key: 'r',
		value: 'r'
	},
	{
		key: 's',
		value: 's'
	},
	{
		key: 't',
		value: 't'
	},
	{
		key: 'u',
		value: 'u'
	},
	{
		key: 'v',
		value: 'v'
	},
	{
		key: 'w',
		value: 'w'
	},
	{
		key: 'x',
		value: 'x'
	},
	{
		key: 'y',
		value: 'y'
	},
	{
		key: 'z',
		value: 'z'
	}
];

export { LETTERS };
