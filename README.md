# Dijital Hafıza Merkezi


Bu projede PNPM (https://pnpm.io/tr/) kullanılmıştır. PNPM'yi makineye kurmak için:

```bash
npm i -g pnpm
```

## Projeyi Ayağa Kaldırma

```bash
# Kurmak için
pnpm install

# Çalıştırmak için
pnpm dev

# Tarayıcıyı da açmak için
pnpm dev -- --open

# Build almak için
pnpm build
```

## Storybook'u Çalıştırma

```bash
pnpm storybook
```
