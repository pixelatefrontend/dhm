// import dotenv from 'dotenv';
import preprocess from 'svelte-preprocess';
import path from 'path';
// import precompileIntl from 'svelte-intl-precompile/sveltekit-plugin.js';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import adapterStatic from '@sveltejs/adapter-static';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import adapterVercel from '@sveltejs/adapter-vercel';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import adapterNode from '@sveltejs/adapter-node';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import adapterAuto from '@sveltejs/adapter-auto';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: preprocess({
		preserve: ['ld+json'],
		scss: {
			prependData: '@import "./src/key.scss";',
		},
	}),

	kit: {
		adapter: adapterAuto(),
		vite: {
			server: {
				fs: {
					allow: ['..'],
				},
			},
			// plugins: [precompileIntl('locales')],
			resolve: {
				alias: {
					$components: path.resolve('./src/components'),
					$assets: path.resolve('./src/assets'),
					$styles: path.resolve('./src/styles'),
					$stores: path.resolve('./src/stores'),
					$utils: path.resolve('./src/utils'),
					$locales: path.resolve('./locales'),
					$nm: path.resolve('./node_modules'),
				},
			},
			css: {
				preprocessorOptions: {
					scss: {
						additionalData: '@import "./src/key.scss";',
					},
				},
			},
		},
	},
};

export default config;
